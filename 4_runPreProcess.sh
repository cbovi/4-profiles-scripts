sudo chown -R ubuntu:ubuntu $FOUR_PROFILES_SCRIPTS
sudo chmod -R 755 $FOUR_PROFILES_SCRIPTS
 
free mem

echo ""
echo "Enter number of gigabytes of ram to be used for pre processing (example \"3g\" - do not enter a value greater than available column):"
read mem
echo ""

echo "Enter input file name to pre-process:"
echo "ls $P"
ls $P
echo ""
read f

cd $P
java -Xms$mem -Xmx$mem -jar $JAR_DIR/$PREPROCESS_JAR $f $PREPROCESS_SUFFIX 
mv $f$PREPROCESS_SUFFIX $I

echo ""
echo "Output files:"
ls $I/$f$PREPROCESS_SUFFIX
ls $P/$f$PREPROCESS_SUFFIX-log
ls $P/$f$PREPROCESS_SUFFIX-edgesCount

sudo chown -R ubuntu:ubuntu $FOUR_PROFILES_SCRIPTS
sudo chmod -R 755 $FOUR_PROFILES_SCRIPTS

