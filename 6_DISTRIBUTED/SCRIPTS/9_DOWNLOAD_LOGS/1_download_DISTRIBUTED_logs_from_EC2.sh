echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""


mkdir -p $HOME/EC2_LOGS/
mkdir -p $HOME/EC2_LOGS/DISTRIBUTED

sudo chown -R ubuntu:ubuntu $HOME/EC2_LOGS/DISTRIBUTED
sudo chmod -R 755 $HOME/EC2_LOGS/DISTRIBUTED

echo "\nEnter session directory name in which will be downloaded logs (enter same session directory name used for 4run_giraph_distributed.sh):"
read x

dateDir="$(date +'%Y-%m-%d')"
dateDirLog="$(date +'%Y-%m-%d_%H-%M-%S')"

nomedir="$HOME/EC2_LOGS/DISTRIBUTED/$dataDir/$dateDirLog--$x"

mkdir -p $nomedir

cd $nomedir
mkdir -p namenode 
scp -rp namenode:/usr/local/hadoop-$HADOOP_VERSION/logs namenode 


for i in $(seq 1 $DATANODES_NUMBER);
do
	echo "datanode$i"
	mkdir -p datanode$i
	scp -rp datanode$i:/usr/local/hadoop-$HADOOP_VERSION/logs datanode$i
done

cd ../..



echo "Digitare:"
echo "1, per istruzioni generiche"
echo "2, per istruzioni specifiche per il mio notebook"
read i


if [ $i = "2" ] 
then
	#istruzioni specifiche per il mio notebook in cui il workspace si trova nella home dell'utente user1 mentre 
	#la directory 4-profiles-scripts si trova nella home dell'utente ubuntu
	sudo chown -R user1:user1 $HOME/EC2_LOGS/DISTRIBUTED
	sudo chmod -R 755 $HOME/EC2_LOGS/DISTRIBUTED
fi


echo "\n\nLogs downloaded in $nomedir"
