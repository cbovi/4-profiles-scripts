echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""

mkdir -p $HOME/EC2_LOGS/
mkdir -p $HOME/EC2_LOGS/STANDALONE

sudo chown -R ubuntu:ubuntu $HOME/EC2_LOGS/STANDALONE
sudo chmod -R 755 $HOME/EC2_LOGS/STANDALONE

echo "Inserire nome directory per i LOGS:"
read x

data2="$(date +'%Y-%m-%d')"
data="$(date +'%Y-%m-%d_%H-%M-%S')"

nomedir="$HOME/EC2_LOGS/STANDALONE/$data2/$data_SESSION/$data--$x"

mkdir -p $nomedir

cd $nomedir
mkdir -p namenode 


scp -rp namenode:/usr/local/hadoop-$HADOOP_VERSION/logs namenode 

cd ../..

sudo chown -R user1:user1 $HOME/EC2_LOGS/STANDALONE
sudo chmod -R 755 $HOME/EC2_LOGS/STANDALONE


echo "\n\nLogs downloaded in $nomedir"
