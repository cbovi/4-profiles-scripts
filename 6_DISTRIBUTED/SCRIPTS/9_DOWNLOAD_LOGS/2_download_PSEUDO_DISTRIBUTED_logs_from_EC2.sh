echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""

mkdir -p $HOME/EC2_LOGS/
mkdir -p $HOME/EC2_LOGS/PSEUDO_DISTRIBUTED

sudo chown -R ubuntu:ubuntu $HOME/EC2_LOGS/PSEUDO_DISTRIBUTED
sudo chmod -R 755 $HOME/EC2_LOGS/PSEUDO_DISTRIBUTED

echo "\nEnter session directory name in which will be downloaded logs (enter same session directory name used for 4run_giraph_distributed.sh):"
read x

data2="$(date +'%Y-%m-%d')"
data="$(date +'%Y-%m-%d_%H-%M-%S')"

nomedir="$HOME/EC2_LOGS/PSEUDO_DISTRIBUTED/$data2/$data--$x"

mkdir -p $nomedir

cd $nomedir
mkdir -p namenode 


scp -rp namenode:/usr/local/hadoop-$HADOOP_VERSION/logs namenode 

#cd ../..
echo "Digitare:"
echo "1, per impostare come owner l'utente user1 sulla cartella $HOME/EC2_LOGS (impostazione specifica del mio notebook)" 
echo "2, per non eseguire nessuna impostazione"
read n

if [ $n = "1" ]
then
	sudo chown -R user1:user1 $HOME/EC2_LOGS/PSEUDO_DISTRIBUTED
	sudo chmod -R 755 $HOME/EC2_LOGS/PSEUDO_DISTRIBUTED
fi


echo "\n\nLogs downloaded in $nomedir"
