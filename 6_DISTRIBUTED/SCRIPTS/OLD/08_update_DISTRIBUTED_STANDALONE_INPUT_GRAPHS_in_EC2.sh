echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""


cd $FOUR_PROFILES_SCRIPTS
ssh namenode "mkdir -p $FOUR_PROFILES_SCRIPTS"

rm 0_SETUP.zip
zip -r 0_SETUP.zip 0_SETUP
scp 0_SETUP.zip namenode:$FOUR_PROFILES_SCRIPTS

rm 1_STANDALONE.zip
zip -r 1_STANDALONE.zip 1_STANDALONE
scp 1_STANDALONE.zip namenode:$FOUR_PROFILES_SCRIPTS

rm 2_DISTRIBUTED.zip
zip -r 2_DISTRIBUTED.zip 2_DISTRIBUTED
scp 2_DISTRIBUTED.zip namenode:$FOUR_PROFILES_SCRIPTS



scp $FOUR_PROFILES_JAR namenode:$FOUR_PROFILES_SCRIPTS
scp $PREPROCESS_JAR namenode:$FOUR_PROFILES_SCRIPTS
scp $FOUR_PROFILES_JAR namenode:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib




echo "Enter:"
echo "1 for distributed mode"
echo "2 for pseudo-distributed mode" 
read m

if [ $m = "1" ]
then
	scp $FOUR_PROFILES_JAR datanode1:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
	scp $FOUR_PROFILES_JAR datanode2:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
	scp $FOUR_PROFILES_JAR datanode3:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
	scp $FOUR_PROFILES_JAR datanode4:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
	scp $FOUR_PROFILES_JAR datanode5:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
fi

ssh namenode "rm -R $FOUR_PROFILES_SCRIPTS/0_SETUP"
ssh namenode "unzip $FOUR_PROFILES_SCRIPTS/0_SETUP.zip -d $FOUR_PROFILES_SCRIPTS"

ssh namenode "rm -R $FOUR_PROFILES_SCRIPTS/1_STANDALONE"
ssh namenode "unzip $FOUR_PROFILES_SCRIPTS/1_STANDALONE.zip -d $FOUR_PROFILES_SCRIPTS"

ssh namenode "rm -R $FOUR_PROFILES_SCRIPTS/2_DISTRIBUTED"
ssh namenode "unzip $FOUR_PROFILES_SCRIPTS/2_DISTRIBUTED.zip -d $FOUR_PROFILES_SCRIPTS"

echo "Enter"
echo "- 1 for update INPUT_GRAPHS directory on namenode"
echo "- other number to skip update INPUT_GRAPHS"
read n

if [ $n = "1" ]
then
	rm INPUT_GRAPHS.zip
	zip -r INPUT_GRAPHS.zip INPUT_GRAPHS
	scp INPUT_GRAPHS.zip namenode:$FOUR_PROFILES_SCRIPTS

	ssh namenode "rm -R $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS"
	ssh namenode "unzip $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS.zip -d $FOUR_PROFILES_SCRIPTS"
fi

