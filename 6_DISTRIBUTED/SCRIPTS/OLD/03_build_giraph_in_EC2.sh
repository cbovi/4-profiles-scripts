echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""

#https://stackoverflow.com/questions/33728752/building-giraph-with-hadoop

#namenode
scp ../CONF/giraph/pom.xml namenode:$GIRAPH_GIT/giraph
scp ../CONF/giraph/GiraphYarnClient.java namenode:$GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/yarn
scp ../CONF/giraph/2build_giraph_yarn.sh namenode:$GIRAPH_GIT
scp ../CONF/giraph/3build_giraph_hadoop2.sh namenode:$GIRAPH_GIT


#datanode1
scp ../CONF/giraph/pom.xml datanode1:$GIRAPH_GIT/giraph
scp ../CONF/giraph/GiraphYarnClient.java datanode1:$GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/yarn
scp ../CONF/giraph/2build_giraph_yarn.sh datanode1:$GIRAPH_GIT


#datanode2
scp ../CONF/giraph/pom.xml datanode2:$GIRAPH_GIT/giraph
scp ../CONF/giraph/GiraphYarnClient.java datanode2:$GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/yarn
scp ../CONF/giraph/2build_giraph_yarn.sh datanode2:$GIRAPH_GIT


#datanode3
scp ../CONF/giraph/pom.xml datanode3:$GIRAPH_GIT/giraph
scp ../CONF/giraph/GiraphYarnClient.java datanode3:$GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/yarn
scp ../CONF/giraph/2build_giraph_yarn.sh datanode3:$GIRAPH_GIT


#datanode4
scp ../CONF/giraph/pom.xml datanode4:$GIRAPH_GIT/giraph
scp ../CONF/giraph/GiraphYarnClient.java datanode4:$GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/yarn
scp ../CONF/giraph/2build_giraph_yarn.sh datanode4:$GIRAPH_GIT


#datanode5
scp ../CONF/giraph/pom.xml datanode5:$GIRAPH_GIT/giraph
scp ../CONF/giraph/GiraphYarnClient.java datanode5:$GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/yarn
scp ../CONF/giraph/2build_giraph_yarn.sh datanode5:$GIRAPH_GIT

