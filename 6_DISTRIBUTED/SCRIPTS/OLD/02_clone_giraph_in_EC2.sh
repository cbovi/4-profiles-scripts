echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""

#https://stackoverflow.com/questions/33728752/building-giraph-with-hadoop

#namenode
ssh namenode "mkdir $HOME/Downloads"
ssh namenode "mkdir $GIRAPH_GIT"
scp ../CONF/giraph/1clone_giraph_from_git_repository.sh namenode:$GIRAPH_GIT


#datanode1
ssh namenode "mkdir $HOME/Downloads"
ssh namenode "mkdir $GIRAPH_GIT"
scp ../CONF/giraph/1clone_giraph_from_git_repository.sh datanode1:$GIRAPH_GIT


#datanode2
ssh namenode "mkdir $HOME/Downloads"
ssh namenode "mkdir $GIRAPH_GIT"
scp ../CONF/giraph/1clone_giraph_from_git_repository.sh datanode2:$GIRAPH_GIT


#datanode3
ssh namenode "mkdir $HOME/Downloads"
ssh namenode "mkdir $GIRAPH_GIT"
scp ../CONF/giraph/1clone_giraph_from_git_repository.sh datanode3:$GIRAPH_GIT


#datanode4
ssh namenode "mkdir $HOME/Downloads"
ssh namenode "mkdir $GIRAPH_GIT"
scp ../CONF/giraph/1clone_giraph_from_git_repository.sh datanode4:$GIRAPH_GIT


#datanode5
ssh namenode "mkdir $HOME/Downloads"
ssh namenode "mkdir $GIRAPH_GIT"
scp ../CONF/giraph/1clone_giraph_from_git_repository.sh datanode5:$GIRAPH_GIT

