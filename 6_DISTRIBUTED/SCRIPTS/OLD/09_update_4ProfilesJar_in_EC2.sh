echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""

cd $FOUR_PROFILES_SCRIPTS
scp $FOUR_PROFILES_JAR namenode:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
scp $FOUR_PROFILES_JAR datanode1:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
scp $FOUR_PROFILES_JAR datanode2:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
scp $FOUR_PROFILES_JAR datanode3:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
scp $FOUR_PROFILES_JAR datanode4:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
scp $FOUR_PROFILES_JAR datanode5:/usr/local/hadoop-$HADOOP_VERSION/share/hadoop/yarn/lib
