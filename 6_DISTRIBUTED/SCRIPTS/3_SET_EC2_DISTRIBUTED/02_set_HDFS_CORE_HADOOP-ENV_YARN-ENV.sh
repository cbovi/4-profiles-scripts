echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""

#namenode
scp CONF/namenode/hadoop/hdfs-site.xml namenode:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
scp CONF/namenode/hadoop/masters namenode:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
scp CONF/namenode/hadoop/slaves namenode:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
scp CONF/allnodes/hadoop/core-site.xml namenode:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
scp CONF/allnodes/hadoop/hadoop-env.sh namenode:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
#TODO broken pipe
scp CONF/allnodes/hadoop/yarn-env.sh namenode:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
scp CONF/allnodes/hadoop/yarn namenode:/usr/local/hadoop-$HADOOP_VERSION/bin
scp CONF/allnodes/hadoop/hadoop namenode:/usr/local/hadoop-$HADOOP_VERSION/bin


for i in $(seq 1 $DATANODES_NUMBER);
do
	echo "datanode$i"
	scp CONF/datanodes/hadoop/hdfs-site.xml datanode$i:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	scp CONF/allnodes/hadoop/core-site.xml datanode$i:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	scp CONF/allnodes/hadoop/hadoop-env.sh datanode$i:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	scp CONF/allnodes/hadoop/yarn-env.sh datanode$i:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	scp CONF/allnodes/hadoop/yarn datanode$i:/usr/local/hadoop-$HADOOP_VERSION/bin
	scp CONF/allnodes/hadoop/hadoop datanode$i:/usr/local/hadoop-$HADOOP_VERSION/bin
done

