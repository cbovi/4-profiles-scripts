# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Set Hadoop-specific environment variables here.

# The only required environment variable is JAVA_HOME.  All others are
# optional.  When running a distributed configuration it is best to
# set JAVA_HOME in this file, so that it is correctly defined on
# remote nodes.





# The java implementation to use.
# TESI ############################################################################################################
export JAVA_HOME=/usr/lib/jvm/java-8-oracle







# The jsvc implementation to use. Jsvc is required to run secure datanodes
# that bind to privileged ports to provide authentication of data transfer
# protocol.  Jsvc is not required if SASL is configured for authentication of
# data transfer protocol using non-privileged ports.
#export JSVC_HOME=${JSVC_HOME}

export HADOOP_CONF_DIR=${HADOOP_CONF_DIR:-"/etc/hadoop"}

# Extra Java CLASSPATH elements.  Automatically insert capacity-scheduler.
for f in $HADOOP_HOME/contrib/capacity-scheduler/*.jar; do
  if [ "$HADOOP_CLASSPATH" ]; then
    export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:$f
  else
    export HADOOP_CLASSPATH=$f
  fi
done

# The maximum amount of heap to use, in MB. Default is 1000.
#export HADOOP_HEAPSIZE=
#export HADOOP_NAMENODE_INIT_HEAPSIZE=""

# Enable extra debugging of Hadoop's JAAS binding, used to set up
# Kerberos security.
# export HADOOP_JAAS_DEBUG=true

# Extra Java runtime options.  Empty by default.
# For Kerberos debugging, an extended option set logs more invormation
# export HADOOP_OPTS="-Djava.net.preferIPv4Stack=true -Dsun.security.krb5.debug=true -Dsun.security.spnego.debug"

#TESI##########################################################################################################################
#https://www.quora.com/Why-does-this-issue-EndOfStreamException-Unable-to-read-additional-data-from-client-sessionid-0x25e98daaea80052-likely-client-has-closed-socket-occurs-in-ZooKeeper-service
#TODO VERIFICARE SE EFFETTIVAMENTE OCCORRE QUESTA IMPOSTAZIONE POICHE' DALLA LETTURA DELL'ARTICOLO DI FACEBOOK RISULTA CHE LA LIMITAZIONE
#     DI ZOOKEEPER AD 1 MB E' STATA SUPERATA NON TRAMITE L'IMPOSTAZIONE RIPORTATA DI SEGUITO MA UTILIZZANDO NETTY
#-Djute.maxbuffer=20000000 
#TESI##########################################################################################################################
export HADOOP_OPTS="$HADOOP_OPTS -Djava.net.preferIPv4Stack=true -Djute.maxbuffer=20000000"

# Command specific options appended to HADOOP_OPTS when specified
export HADOOP_NAMENODE_OPTS="-Djute.maxbuffer=20000000 -Dhadoop.security.logger=${HADOOP_SECURITY_LOGGER:-INFO,RFAS} -Dhdfs.audit.logger=${HDFS_AUDIT_LOGGER:-INFO,NullAppender} $HADOOP_NAMENODE_OPTS"








#TESI - GARBAGE COLLECTOR SETTINGS ##############################################################################################
#1) UTILIZZO DEL GARBAGE COLLECTOR ConcMarkSweepGC
#web.cs.wpi.edu/~cs525/f13b-EAR/cs525-homepage/lectures/lectures-hadoop/giraph.pptx
#https://community.hortonworks.com/articles/14170/namenode-garbage-collection-configuration-best-pra.html
#https://community.hortonworks.com/questions/121935/regarding-namenode-heap-size.html
export HADOOP_NAMENODE_OPTS="-XX:+UseG1GC -Xms2G -Xmx2G $HADOOP_NAMENODE_OPTS"

#https://stackoverflow.com/questions/53467710/usage-of-xxuseparallelgc-in-hadoop-datanodes
#export HADOOP_DATANODE_OPTS="-XX:+UseG1GC -Xms3G -Xmx3G $HADOOP_DATANODE_OPTS"

#2) UTILIZZO DEL GARBAGE COLLECTOR G1GC (G1GC E' IL SUCCESSORE DI ConcMarkSweppGC)
#https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.6.5/bk_hdfs-administration/content/ch_g1gc_garbage_collector_tech_preview.html
#https://docs.hortonworks.com/HDPDocuments/HDP2/HDP-2.6.5/bk_command-line-installation/content/configuring-namenode-heap-size.html
#https://docs.oracle.com/cd/E40972_01/doc.70/e40973/cnf_jvmgc.htm#autoId2
#https://www.oracle.com/technetwork/articles/java/g1gc-1984535.html


#3) TODO
#https://www.slideshare.net/ScottSeighman/tuning-java-for-big-data


#4) TODO Sembrerebbe una configurazione base l'utilizzo del solo parametro -XX:+UseParallelGC
#https://hadoop.apache.org/docs/r2.8.4/hadoop-project-dist/hadoop-common/ClusterSetup.html
#export HADOOP_NAMENODE_OPTS="-XX:+UseParallelGC $HADOOP_NAMENODE_OPTS"

#5) TODO Probabilmente non occorre configurare il garbage collector anche in HADOOP_DATANODE_OPTS
#https://stackoverflow.com/questions/53467710/usage-of-xxuseparallelgc-in-hadoop-datanodes
#export HADOOP_DATANODE_OPTS="-XX:+UseParallelGC $HADOOP_DATANODE_OPTS"


################################################################################################################################








export HADOOP_DATANODE_OPTS="-Djute.maxbuffer=20000000 -Dhadoop.security.logger=ERROR,RFAS $HADOOP_DATANODE_OPTS"

export HADOOP_SECONDARYNAMENODE_OPTS="-Dhadoop.security.logger=${HADOOP_SECURITY_LOGGER:-INFO,RFAS} -Dhdfs.audit.logger=${HDFS_AUDIT_LOGGER:-INFO,NullAppender} $HADOOP_SECONDARYNAMENODE_OPTS"

export HADOOP_NFS3_OPTS="$HADOOP_NFS3_OPTS"
export HADOOP_PORTMAP_OPTS="-Xmx512m $HADOOP_PORTMAP_OPTS"

# The following applies to multiple commands (fs, dfs, fsck, distcp etc)
export HADOOP_CLIENT_OPTS="$HADOOP_CLIENT_OPTS"
# set heap args when HADOOP_HEAPSIZE is empty
if [ "$HADOOP_HEAPSIZE" = "" ]; then
  export HADOOP_CLIENT_OPTS="-Xmx512m $HADOOP_CLIENT_OPTS"
fi
#HADOOP_JAVA_PLATFORM_OPTS="-XX:-UsePerfData $HADOOP_JAVA_PLATFORM_OPTS"

# On secure datanodes, user to run the datanode as after dropping privileges.
# This **MUST** be uncommented to enable secure HDFS if using privileged ports
# to provide authentication of data transfer protocol.  This **MUST NOT** be
# defined if SASL is configured for authentication of data transfer protocol
# using non-privileged ports.
export HADOOP_SECURE_DN_USER=${HADOOP_SECURE_DN_USER}





# TESI ############################################################################################################
export HADOOP_ROOT_LOGGER=INFO,RFA






# Where log files are stored.  $HADOOP_HOME/logs by default.
#export HADOOP_LOG_DIR=${HADOOP_LOG_DIR}/$USER

# Where log files are stored in the secure data environment.
#export HADOOP_SECURE_DN_LOG_DIR=${HADOOP_LOG_DIR}/${HADOOP_HDFS_USER}

###
# HDFS Mover specific parameters
###
# Specify the JVM options to be used when starting the HDFS Mover.
# These options will be appended to the options specified as HADOOP_OPTS
# and therefore may override any similar flags set in HADOOP_OPTS
#
# export HADOOP_MOVER_OPTS=""

###
# Advanced Users Only!
###

# The directory where pid files are stored. /tmp by default.
# NOTE: this should be set to a directory that can only be written to by 
#       the user that will run the hadoop daemons.  Otherwise there is the
#       potential for a symlink attack.
export HADOOP_PID_DIR=${HADOOP_PID_DIR}
export HADOOP_SECURE_DN_PID_DIR=${HADOOP_PID_DIR}

# A string representing this instance of hadoop. $USER by default.
export HADOOP_IDENT_STRING=$USER
