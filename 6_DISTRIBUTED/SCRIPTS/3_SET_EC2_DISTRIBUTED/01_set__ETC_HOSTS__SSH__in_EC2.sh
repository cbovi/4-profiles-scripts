echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""

#namenode
#le due seguenti istruzioni vengono usate per aggiornare il file /etc/hosts
scp CONF/allnodes/linux/hosts namenode:/home/ubuntu
ssh namenode "sudo mv /home/ubuntu/hosts /etc"
#fine istruzioni per aggiornare /etc/hosts
scp CONF/allnodes/ssh/config namenode:/home/ubuntu/.ssh
scp CONF/allnodes/ssh/sclanofrancescoec2.pem namenode:/home/ubuntu/.ssh
ssh namenode "sudo chmod 600 /home/ubuntu/.ssh/sclanofrancescoec2.pem"

#1)
#ssh namenode 'ssh-keygen -f ~/.ssh/id_rsa -t rsa -P ""'

#FROM https://blog.insightdatascience.com/spinning-up-a-free-hadoop-cluster-step-by-step-c406d56bae42
#On the NameNode we can create the public fingerprint, found in ~/.ssh/id_rsa.pub, and add it first to the NameNode’s authorized_keys (vedi il comando 2)

#Il comando sopra è stato commentato poiché deve essere eseguito solo la prima volta in assoluto dopo che è stato creato il namenode altrimenti verrebbe generata inutilmente un'altra chiave (come mostrato dall'output sotto riportato)
# Generating public/private rsa key pair.
#/home/ubuntu/.ssh/id_rsa already exists.
#Overwrite (y/n)? n

#2)
#ssh namenode 'cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys'
#Anche il comando sopra è stato commentato poiché deve essere eseguito solo la prima volta in assoluto dopo che è stato creato il namenode altrimenti verrebbe inserita nel file .ssh/authorized_keys una chiave duplicata (come mostra l'esempio sotto ottenuto eseguendo il comando sopra e poi eseguendo il cat su .ssh/authorized_keys)
#ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCLXwFoleaJqy+ePyfvrNe0fDUBvYOzHU6xr3OtVOJSLHwA8n91gUBotuAcLM5CoS/lkcPl+peltvj74bKcvkbaMvhjBbIXSL+2dS9bDG7zV+ee75at65EVUqN6tLZ2tzlmjCymAZpUttBxLHiZkw66i6gW0xyQ1EYz8mCRCryVU/ywG6x4627iXCXZNoAkOUGHE+24QoT3e/LjyxKmYOYDiTc5d6l/m+gDHVOjRlhYNGV0VAChSW/w0NPDsE3FbZ7/MWybUH3KvGdrJeKqRiNtI5TzKJ50aYE9FG8luh8i8mmLTYwge/rRBwC8EajPhIgOEwKqfQ/4z3+6Rhho9qI1 sclano.francesco.ec2
#ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKMhyCjzhSqApYrEdjmP8P3RF3q9EabR/4Cpibaaiue/duQ+/4ZepMxgtOUOCIxxDWPGF5fqSjix4x8UqkTGBdMOK09RqXauw1YYVTQ0Y9z3bV/wlo0I/EEsqrWpqJ4kx8HNNC4afFh9rBSG41p0rd2h+pq9LJ0Y+x69v13ZxwiaI4RrVaBd03C7k/tvVsj6xrmPISf5fI6yX6Xb5j0Q4RnVdmSeY1JEP7oUXK/mmtTic+8uE18OPE1NN9EJEK9ET67N5DFTyktbEPb4eDZ+r9qqL+FaS645EpuPC/3SvkH8HjYJpkHf/OTaOOVUv5hhca3p8Nw4zSzvXoSNzSu4nP ubuntu@ip-172-31-20-241
#ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKMhyCjzhSqApYrEdjmP8P3RF3q9EabR/4Cpibaaiue/duQ+/4ZepMxgtOUOCIxxDWPGF5fqSjix4x8UqkTGBdMOK09RqXauw1YYVTQ0Y9z3bV/wlo0I/EEsqrWpqJ4kx8HNNC4afFh9rBSG41p0rd2h+pq9LJ0Y+x69v13ZxwiaI4RrVaBd03C7k/tvVsj6xrmPISf5fI6yX6Xb5j0Q4RnVdmSeY1JEP7oUXK/mmtTic+8uE18OPE1NN9EJEK9ET67N5DFTyktbEPb4eDZ+r9qqL+FaS645EpuPC/3SvkH8HjYJpkHf/OTaOOVUv5hhca3p8Nw4zSzvXoSNzSu4nP ubuntu@ip-172-31-20-241

#3)
#FROM https://blog.insightdatascience.com/spinning-up-a-free-hadoop-cluster-step-by-step-c406d56bae42
#Now we need to copy the public fingerprint to each DataNode’s~/.ssh/authorized_keys. This should enable the password-less SSH capabilities from the NameNode to any DataNode.


#I seguenti comandi vanno eseguiti solamente la prima volta in assoluto dopo la creazione di un nuovo datanode
#(il comando è stato testato e funziona anche se ha un ssh dentro un altro ssh)
#ssh namenode "cat ~/.ssh/id_rsa.pub | ssh datanode1 'cat >> ~/.ssh/authorized_keys'"
#ssh namenode "cat ~/.ssh/id_rsa.pub | ssh datanode2 'cat >> ~/.ssh/authorized_keys'"
#ssh namenode "cat ~/.ssh/id_rsa.pub | ssh datanode3 'cat >> ~/.ssh/authorized_keys'"
#ssh namenode "cat ~/.ssh/id_rsa.pub | ssh datanode4 'cat >> ~/.ssh/authorized_keys'"
#ssh namenode "cat ~/.ssh/id_rsa.pub | ssh datanode5 'cat >> ~/.ssh/authorized_keys'"


#FROM https://blog.insightdatascience.com/spinning-up-a-free-hadoop-cluster-step-by-step-c406d56bae42
#We can check this by trying to SSH into any of the DataNodes from the NameNode. You may still be prompted if you are sure you want to connect (in verità no perché nel file .ssh/config è stato aggiunto ), but there should be no password requirement.



for i in $(seq 1 $DATANODES_NUMBER);
do
	echo "datanode$i"
	scp CONF/allnodes/linux/hosts datanode$i:/home/ubuntu
	ssh datanode$i "sudo mv /home/ubuntu/hosts /etc"
	scp CONF/allnodes/ssh/config datanode$i:/home/ubuntu/.ssh
	scp CONF/allnodes/ssh/sclanofrancescoec2.pem datanode$i:/home/ubuntu/.ssh
	ssh datanode$i "sudo chmod 600 /home/ubuntu/.ssh/sclanofrancescoec2.pem"
done



