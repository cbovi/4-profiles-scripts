echo "ATTENZIONE - Verificare di aver aggiornato:"
echo "- il firewall di EC2 con l'IP corrente"
echo "- i public DNS di tutti i nodi del cluster EC2 nel file .ssh/config"
echo ""
echo "DATANODES NUMBER = $DATANODES_NUMBER"
echo ""

scp CONF/allnodes/hadoop/07_c3.8xlarge_60GB_32CORE_3CONTAINER/mapred-site.xml namenode:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
scp CONF/allnodes/hadoop/07_c3.8xlarge_60GB_32CORE_3CONTAINER/yarn-site.xml namenode:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop

for i in $(seq 1 $DATANODES_NUMBER);
do
	echo "datanode$i"
	scp CONF/allnodes/hadoop/07_c3.8xlarge_60GB_32CORE_3CONTAINER/mapred-site.xml datanode$i:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	scp CONF/allnodes/hadoop/07_c3.8xlarge_60GB_32CORE_3CONTAINER/yarn-site.xml datanode$i:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
done
