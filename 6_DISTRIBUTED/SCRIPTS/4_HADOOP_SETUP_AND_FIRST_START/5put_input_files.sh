echo "\nmkdir /user/ubuntu/INPUT_GRAPHS <----------------------------------------------------------------------------"
hadoop fs -mkdir -p /user/ubuntu/INPUT_GRAPHS

#$FOUR_PROFILES_SCRIPTS=/home/ubuntu/4-profiles-scripts

echo "\nput inputArticolo4Profiles.txt <-----------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/inputArticolo4Profiles.txt /user/ubuntu/INPUT_GRAPHS

echo "\nput inputInventato.txt <-------------------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/inputInventato.txt /user/ubuntu/INPUT_GRAPHS

echo "\nput p2p-Gnutella04.txt-processed <---------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/p2p-Gnutella04.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput p2p-Gnutella05.txt-processed <---------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/p2p-Gnutella05.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput p2p-Gnutella06.txt-processed <---------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/p2p-Gnutella06.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput p2p-Gnutella08.txt-processed <---------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/p2p-Gnutella08.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput p2p-Gnutella09.txt-processed <---------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/p2p-Gnutella09.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput p2p-Gnutella31.txt-processed <---------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/p2p-Gnutella31.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput cit-HepTh.txt-processed <--------------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/cit-HepTh.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput com-amazon.ungraph.txt-processed <-----------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/com-amazon.ungraph.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput email-Enron.txt-processed <------------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/email-Enron.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput email-EuAll.txt-processed <------------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/email-EuAll.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput loc-brightkite_edges.txt-processed <---------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/loc-brightkite_edges.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput web-NotreDame.txt-processed <----------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/web-NotreDame.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput soc-LiveJournal1.txt-processed <-------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/soc-LiveJournal1.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput soc-Epinions1.txt-processed <----------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/soc-Epinions1.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput amazon0302.txt-processed <-------------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/amazon0302.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput com-dblp.ungraph.txt-processed <-------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/com-dblp.ungraph.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput HU_edges.txt-processed <---------------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/HU_edges.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput soc-sign-Slashdot090221.txt <----------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/soc-sign-Slashdot090221.txt-processed /user/ubuntu/INPUT_GRAPHS

echo "\nput soc-sign-Slashdot090216.txt <----------------------------------------------------------------------------"
hadoop fs -put $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/soc-sign-Slashdot090216.txt-processed /user/ubuntu/INPUT_GRAPHS




