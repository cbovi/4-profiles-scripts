#1
#I seguenti comandi cp non sarammp più necessari una volta che sarò riuscita a far funzionare le proprietà yarn.application.classpath e mapred.application.classpath nei rispettivi files yarn-site.xml e mapred-site.xml

#INIZIO COMANDI cp NON PIU' NECESSARI
#https://stackoverflow.com/questions/29001491/could-not-find-or-load-main-class-org-apache-giraph-yarn-giraphapplicationmaster
#Per risolvere il seguente errore risultante dal log di gam:
#cat /usr/local/hadoop-2.8.4/logs/userlogs/application_1542541600222_0001/container_1542541600222_0001_02_000001/gam-stderr.log
#Error: Could not find or load main class org.apache.giraph.yarn.GiraphApplicationMaster

#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/*
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/*

#sudo cp $HOME/Downloads/HADOOP_DOWNLOADS/hadoop-2.8.4/share/hadoop/yarn/lib/*.jar $HADOOP_HOME/share/hadoop/yarn/lib/
#sudo cp $HOME/Downloads/HADOOP_DOWNLOADS/hadoop-2.8.4/share/hadoop/common/lib/*.jar $HADOOP_HOME/share/hadoop/common/lib/

sudo cp $JAR_DIR/$FOUR_PROFILES_JAR $HADOOP_HOME/share/hadoop/yarn/lib/
#TODO sudo cp ~/Downloads/GIRAPH_GIT$/giraph/giraph-core/target/giraph-1.3.0-SNAPSHOT-for-hadoop-2.8.4-jar-with-dependencies.jar $HADOOP_HOME/share/hadoop/yarn/lib/
sudo cp $GIRAPH_HOME/*.jar $HADOOP_HOME/share/hadoop/yarn/lib/
sudo cp $GIRAPH_HOME/lib/*.jar $HADOOP_HOME/share/hadoop/yarn/lib/
#FINE COMANDI cp NON PIU' NECESSARI


############################################################################################
#soluzione per rimuovere tutti i jar duplicati


#netty-all-4.0.14.Final.jar
sudo cp CONF/allnodes/giraph/netty-all-4.1.31.Final.jar $HADOOP_HOME/share/hadoop/common/lib/
sudo cp CONF/allnodes/giraph/netty-all-4.1.31.Final.jar $HADOOP_HOME/share/hadoop/yarn/lib/
sudo rm $HADOOP_HOME/share/hadoop/common/lib/netty-all-4.0.14.Final.jar
sudo rm $HADOOP_HOME/share/hadoop/common/lib/netty-all-4.0.14.Final.jar
sudo rm $HADOOP_HOME/share/hadoop/common/lib/netty-3.6.2.Final.jar
sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/netty-3.6.2.Final.jar


#zookeeper-3.4.6.jar
sudo cp CONF/allnodes/giraph/zookeeper-3.4.13.jar $HADOOP_HOME/share/hadoop/common/lib/
sudo cp CONF/allnodes/giraph/zookeeper-3.4.13.jar $HADOOP_HOME/share/hadoop/yarn/lib/
sudo rm $HADOOP_HOME/share/hadoop/common/lib/zookeeper-3.4.6.jar
sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/zookeeper-3.4.6.jar
sudo rm $HADOOP_HOME/share/hadoop/common/lib/zookeeper-3.4.5.jar
sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/zookeeper-3.4.5.jar


#antlr-3.4.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/antlr-2.7.7.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/antlr-2.7.7.jar

#asm-5.0.4.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/asm-3.2.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/asm-3.2.jar

#commons-codec-1.8.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/commons-codec-1.4.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/commons-codec-1.4.jar

#commons-io-2.4.jar 
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/commons-io-2.1.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/commons-io-2.1.jar

#commons-lang-2.6.jar
#commons-lang3-3.4.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/
 
#commons-logging-1.1.3.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/commons-logging-1.1.1.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/commons-logging-1.1.1.jar

#commons-math-2.2.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/commons-math-2.1.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/commons-math-2.1.jar
 
#jackson-core-asl-1.9.2.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/jackson-core-asl-1.9.13.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/jackson-core-asl-1.9.13.jar
 
#jackson-mapper-asl-1.9.2.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/jackson-mapper-asl-1.9.13.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/jackson-mapper-asl-1.9.13.jar
 
#jaxb-impl-2.2.4-1.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/jaxb-impl-2.2.3-1.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/jaxb-impl-2.2.3-1.jar

#jersey-core-1.9.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/jersey-core-1.17.jar
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/jersey-core-1.17.jar
 
#servlet-api-2.5-20081211.jar
#servlet-api-2.5.jar
#sudo rm $HADOOP_HOME/share/hadoop/common/lib/
#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/
 
############################################################################################


#2
#soluzione per far funzionare outOfCore
sudo cp $GIRAPH_HOME/lib/guava-18.0.jar $HADOOP_HOME/share/hadoop/common/lib/
sudo cp $GIRAPH_HOME/lib/guava-18.0.jar $HADOOP_HOME/share/hadoop/yarn/lib/
sudo rm $HADOOP_HOME/share/hadoop/common/lib/guava-11.0.2.jar
sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/guava-11.0.2.jar


#3
#soluzione per evitare che in continuazione venga stampato a video il messaggio: Class path contains multiple SLF4J bindings
#SLF4J: Class path contains multiple SLF4J bindings.
#SLF4J: Found binding in [jar:file:/usr/local/hadoop-2.8.4/share/hadoop/common/lib/slf4j-log4j12-1.7.10.jar!/org/slf4j/impl/#StaticLoggerBinder.class]
#SLF4J: Found binding in [jar:file:/usr/local/hadoop-2.8.4/share/hadoop/yarn/lib/slf4j-log4j12-1.7.6.jar!/org/slf4j/impl/#StaticLoggerBinder.class]
#SLF4J: See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.
#SLF4J: Actual binding is of type [org.slf4j.impl.Log4jLoggerFactory]
sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/slf4j-log4j12-1.7.6.jar
sudo rm $GIRAPH_HOME/lib/slf4j-log4j12-1.7.6.jar



echo "Enter:"
echo "1 for distributed mode"
echo "2 for pseudo-distributed mode"
read m

if [ $m = "1" ]
then
	for i in $(seq 1 $DATANODES_NUMBER);
	do
		echo "\ndatanode$i"

		############################################################################################
		#ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/*"
		#ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/*"
		#ssh datanode$i "sudo cp $HOME/Downloads/HADOOP_DOWNLOADS/hadoop-2.8.4/share/hadoop/yarn/lib/*.jar $HADOOP_HOME/share/hadoop/yarn/lib/"
		#ssh datanode$i "sudo cp $HOME/Downloads/HADOOP_DOWNLOADS/hadoop-2.8.4/share/hadoop/common/lib/*.jar $HADOOP_HOME/share/hadoop/common/lib/"
		############################################################################################

		
		#1
		#istruzione commentata perché non funzionano le proprietà mapreduce/yarn.application.classpath 
		#in mapred-site.xml e yarn-site.xml
		#scp $JAR_DIR/$FOUR_PROFILES_JAR datanode$i:$JAR_DIR
		
		#vedi spiegazioni per il namenode
		scp $JAR_DIR/$FOUR_PROFILES_JAR datanode$i:$HADOOP_HOME/share/hadoop/yarn/lib/
		ssh datanode$i "sudo cp $GIRAPH_HOME/*.jar $HADOOP_HOME/share/hadoop/yarn/lib/"
		ssh datanode$i "sudo cp $GIRAPH_HOME/lib/*.jar $HADOOP_HOME/share/hadoop/yarn/lib/"
		
		#2
		#soluzione per far funzionare outOfCore	
		ssh datanode$i "sudo cp $GIRAPH_HOME/lib/guava-18.0.jar $HADOOP_HOME/share/hadoop/common/lib/"
		ssh datanode$i "sudo cp $GIRAPH_HOME/lib/guava-18.0.jar $HADOOP_HOME/share/hadoop/yarn/lib/"
		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/guava-11.0.2.jar"
		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/guava-11.0.2.jar"
		
		#3
		#soluzione per evitare che in continuazione venga stampato a video il messaggio: Class path contains multiple SLF4J binding
		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/slf4j-log4j12-1.7.6.jar"
		ssh datanode$i "sudo rm $GIRAPH_HOME/lib/slf4j-log4j12-1.7.6.jar"



		#soluzione per rimuovere tutti i jar duplicati
		#antlr-3.4.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/antlr-2.7.7.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/antlr-2.7.7.jar"

		#asm-5.0.4.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/asm-3.2.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/asm-3.2.jar"

		#commons-codec-1.8.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/commons-codec-1.4.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/commons-codec-1.4.jar"

		#commons-io-2.4.jar 
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/commons-io-2.1.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/commons-io-2.1.jar"

		#commons-lang-2.6.jar
		#commons-lang3-3.4.jar
		#sudo rm $HADOOP_HOME/share/hadoop/common/lib/
		#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/
		 
		#commons-logging-1.1.3.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/commons-logging-1.1.1.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/commons-logging-1.1.1.jar"

		#commons-math-2.2.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/commons-math-2.1.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/commons-math-2.1.jar"
		 
		#jackson-core-asl-1.9.2.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/jackson-core-asl-1.9.13.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/jackson-core-asl-1.9.13.jar"
		 
		#jackson-mapper-asl-1.9.2.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/jackson-mapper-asl-1.9.13.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/jackson-mapper-asl-1.9.13.jar"
		 
		#jaxb-impl-2.2.4-1.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/jaxb-impl-2.2.3-1.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/jaxb-impl-2.2.3-1.jar"

		#jersey-core-1.9.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/jersey-core-1.17.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/jersey-core-1.17.jar"
		
#netty-all-4.0.14.Final.jar
scp CONF/allnodes/giraph/netty-all-4.1.31.Final.jar datanode$i:$HADOOP_HOME/share/hadoop/common/lib/
scp CONF/allnodes/giraph/netty-all-4.1.31.Final.jar datanode$i:$HADOOP_HOME/share/hadoop/yarn/lib/
ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/netty-all-4.0.14.Final.jar"
ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/netty-all-4.0.14.Final.jar"
ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/netty-3.6.2.Final.jar"
ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/netty-3.6.2.Final.jar"
		#netty-all-4.0.14.Final.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/netty-3.6.2.Final.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/netty-3.6.2.Final.jar"
		 
		#servlet-api-2.5-20081211.jar
		#servlet-api-2.5.jar
		#sudo rm $HADOOP_HOME/share/hadoop/common/lib/
		#sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/
		 
#zookeeper-3.4.6.jar
scp CONF/allnodes/giraph/zookeeper-3.4.13.jar datanode$i:$HADOOP_HOME/share/hadoop/common/lib/
scp CONF/allnodes/giraph/zookeeper-3.4.13.jar datanode$i:$HADOOP_HOME/share/hadoop/yarn/lib/
ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/zookeeper-3.4.6.jar"
ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/zookeeper-3.4.6.jar"
ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/zookeeper-3.4.5.jar"
ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/zookeeper-3.4.5.jar"
		#zookeeper-3.4.6.jar
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/common/lib/zookeeper-3.4.5.jar"
#		ssh datanode$i "sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/zookeeper-3.4.5.jar"
		############################################################################################


	done
fi
