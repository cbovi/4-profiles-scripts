sudo chown -R ubuntu:ubuntu $S
sudo chmod -R 755 $S

sudo chown -R ubuntu:ubuntu $HADOOP_HOME
sudo chmod -R 755 $HADOOP_HOME

sudo chown -R ubuntu:ubuntu $GIRAPH_HOME
sudo chmod -R 755 $GIRAPH_HOME

#namenode
cd $HADOOP_HOME
rm -R logs
rm -R hdfs
rm -R tmp
mkdir tmp
mkdir hdfs
cd hdfs
mkdir name
mkdir data

sudo chown -R ubuntu:ubuntu $HADOOP_HOME
sudo chmod -R 755 $HADOOP_HOME

sudo chown -R ubuntu:ubuntu $GIRAPH_HOME
sudo chmod -R 755 $GIRAPH_HOME



echo "Enter:"
echo "1 for distributed mode"
echo "2 for pseudo-distributed mode"
read m

if [ $m = "1" ]
then
	for i in $(seq 1 $DATANODES_NUMBER);
	do
		echo "\ndatanode$i"
		ssh datanode$i "rm -R $HADOOP_HOME/logs"
		ssh datanode$i "rm -R $HADOOP_HOME/hdfs"
		ssh datanode$i "rm -R $HADOOP_HOME/tmp"
		ssh datanode$i "mkdir $HADOOP_HOME/tmp"
		ssh datanode$i "mkdir $HADOOP_HOME/hdfs"
		ssh datanode$i "mkdir $HADOOP_HOME/hdfs/name"
		ssh datanode$i "mkdir $HADOOP_HOME/hdfs/data"
		ssh datanode$i "sudo chown -R ubuntu:ubuntu $S"
		ssh datanode$i "sudo chmod -R 755 $S"
		ssh datanode$i "sudo chown -R ubuntu:ubuntu $HADOOP_HOME"
		ssh datanode$i "sudo chmod -R 755 $HADOOP_HOME"
		ssh datanode$i "sudo chown -R ubuntu:ubuntu $GIRAPH_HOME"
		ssh datanode$i "sudo chmod -R 755 $GIRAPH_HOME"
	done
fi






