sudo chown -R ubuntu:ubuntu $S
sudo chmod -R 755 $S

echo "Enter:"
echo "1 to delete previous logs in distributed mode"
echo "2 to delete previous logs in pseudo-distributed mode"
read m

rm -R $HADOOP_HOME/logs

sudo chown -R ubuntu:ubuntu $GIRAPH_HOME
sudo chmod -R 755 $GIRAPH_HOME

sudo chown -R ubuntu:ubuntu $HADOOP_HOME
sudo chmod -R 755 $HADOOP_HOME


if [ $m = "1" ]
then
	for i in $(seq 1 $DATANODES_NUMBER);
	do
		echo "\ndatanode$i"
		ssh datanode$i "rm -R $HADOOP_HOME/logs"
		ssh datanode$i "sudo chown -R ubuntu:ubuntu $S"
		ssh datanode$i "sudo chmod -R 755 $S"
		ssh datanode$i "sudo chown -R ubuntu:ubuntu $HADOOP_HOME"
		ssh datanode$i "sudo chmod -R 755 $HADOOP_HOME"
		ssh datanode$i "sudo chown -R ubuntu:ubuntu $GIRAPH_HOME"
		ssh datanode$i "sudo chmod -R 755 $GIRAPH_HOME"
	done
fi
