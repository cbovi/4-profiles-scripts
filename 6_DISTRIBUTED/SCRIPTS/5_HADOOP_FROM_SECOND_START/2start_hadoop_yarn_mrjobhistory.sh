echo "Remember that previous userlogs will be always deleted."
echo "Delete previous hadoop logs? (y/n)"
read p

if [ $p = "y" ]
then
	sh 1rm_logs_chown_chmod_NON_LANCIARE_QUANDO_HADOOP_E_GIA_AVVIATO.sh
fi

echo "\nEnter log level:"
echo "1, DEBUG (ONLY FOR 4 PROFILES AND SMALL INPUT FILES)"
echo "2, INFO"
echo "3, CONSOLE_ERROR"
echo "4, OFF"
echo "5, DEBUG (ONLY FOR HADOOP AND GIRAPH)"
read l
echo ""

if [ $l = "1" ]
then
	sh ../1_SET_LOG_LEVEL/1set_log_level_to_DEBUG_only_for_4PROFILES.sh 
fi

if [ $l = "2" ] 
then
	sh ../1_SET_LOG_LEVEL/2set_log_level_to_INFO.sh
fi

if [ $l = "3" ]
then 
	sh ../1_SET_LOG_LEVEL/3set_log_level_to_CONSOLE_ERROR.sh        
fi

if [ $l = "4" ]
then 
         sh ../1_SET_LOG_LEVEL/4set_log_level_OFF.sh  
fi

if [ $l = "5" ]
then 
         sh ../1_SET_LOG_LEVEL/5set_log_level_to_DEBUG_only_for_HADOOP_GIRAPH.sh
fi



echo "\nstart dfs <--------------------------------------------------------------------"
start-dfs.sh

echo "\nstart yarn <-------------------------------------------------------------------"
start-yarn.sh

#echo "\nstart mr-jobhistory <----------------------------------------------------------"
#mr-jobhistory-daemon.sh start historyserver

echo "\njps <--------------------------------------------------------------------------"
jps

echo "\nCheck http://localhost:8088/cluster/nodes/unhealthy, it must contains 'No data available in table' otherwise check disk space etc.";
