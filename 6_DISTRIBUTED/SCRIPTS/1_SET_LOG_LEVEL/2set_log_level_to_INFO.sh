export DISTRIBUTED_SET_LOG_LEVEL=$DS/1_SET_LOG_LEVEL/

echo "Enter:"
echo "1 to set log level in distributed mode"
echo "2 to set log level in pseudo-distributed mode" 
read m

#namenode
cp $DISTRIBUTED_SET_LOG_LEVEL/CONF/log4j.properties_INFO $C/log4j.properties

if [ $m = "1" ]
then
	for i in $(seq 1 $DATANODES_NUMBER);
	do
		echo "datanode$i"	
		scp $DISTRIBUTED_SET_LOG_LEVEL/CONF/log4j.properties_INFO datanode$i:/usr/local/hadoop-$HADOOP_VERSION/etc/hadoop/log4j.properties
	done
fi
