echo "rm /user/ubuntu/output <----------------------------------------------------------------------------------" 
hadoop fs -rm -r /user/ubuntu/output
 
echo "rm /user/ubuntu/_bsp <------------------------------------------------------------------------------------"
hadoop fs -rm -r /user/ubuntu/_bsp

echo "rm /user/ubuntu/giraph_yarn_jar_cache <-------------------------------------------------------------------"
hadoop fs -rm -r /user/ubuntu/giraph_yarn_jar_cache


hadoop fs -ls /user/ubuntu/INPUT_GRAPHS

echo "\nEnter input file name (without path):"
read x

echo "\nEnter vertex isolated number:"
read ivn

echo "\nEnter mode (3global/4global/4globalFromLocal):"
read z

echo "\nEnter giraph.metrics.enable (true/false):"
read metrics

echo "\nIncremental custom INFO log levels (INFO log level must be previously set):"
echo "0, no custom INFO log"
echo "1, enable log of vertex id, edges, received messages at supersteps 1 and 2"
echo "2, 1 + enable log of memory usage by every vertex at the end of supersteps 1 and 2"
echo "\nEnter custom INFO log level: 0, 1, 2"
read cill

# https://stackoverflow.com/questions/6481005/how-to-obtain-the-number-of-cpus-cores-in-linux-from-the-command-line
echo "\nMax number of availables CORES: $(grep -c ^processor /proc/cpuinfo)"
echo "Enter number of CORES to be used per worker (enter 1 to produce sequential logs in DEBUG mode):"
read core

echo "\n\n\nNOTES ON YARN HEAP FOR SINGLE WORKER:"
echo "Enter corresponding value of"
echo " 	- yarn.scheduler.minimum-allocation-mb property in yarn-site.xml and"
echo " 	- mapreduce.map.memory.mb, yarn.app.mapreduce.am.resource.mb properties in mapred-site.xml"
echo "1) pseudo-distribute mode:"
echo "		my_virtual_machine_4GB_4CORE => 1024 (3072/3 containers)"
echo "		my_phisical_machine_8GB_4CORE => 2048 (6144/3 containers)" 
echo "		t2.xlarge_16GB_4CORE => 4068 (13824/3 containers)" 
echo "2) distributed mode:"
echo "		t2.large_8GB_2CORE => 3072 (6144/2 containers)"
echo "		t2.xlarge_16GB_4CORE => 6912 (13824/2 containers)"
echo "		t2.2xlarge_32GB_8CORE => 15360 (30720/2 containers)"
echo "		c3.8xlarge_60GB_32CORE => 26112 (52224/2 containers)"
echo "		m5.4xlarge_64GB_16CORE => 28416 (56832/2 containers)"
echo "          m5.12xlarge_192GB_48CORE => 83712 (167424 / 2 containers)"

echo "\nEnter yarn heap for single worker in MBs:"
read h

echo "\n\n\nNOTES ON WORKERS NUMBER:"
echo "- Every giraph run needs at least 3 containers:"
echo "     1° container for application master"
echo "     2° container for giraph master"
echo "     3° container for a worker"
echo "- From the workers number depends the partitions number"
echo ""
echo "Hence:"
echo "\n1) pseudo-distribute mode => 1 WORKER"
echo "   Enter only 1 worker since memory has been configured for 3 containers in every type of machine"
echo "\n2) distributed mode => 8 WORKERS"
echo "   Enter 8 workers since:"
echo "   - 5 datanodes on EC2"
echo "   - every datanode configured for max 2 containers in order to reduce messages between datanodes"
echo "   - 2 containers are reserved for application master and giraph master"
echo "   - (5 datanodes * 2 max) - 2 reserved = 8 workers"

echo "\nEnter workers number:"
read worker

# TODO
#echo "\n($worker workers used + 1 yarn application master (gam) + 1 giraph master) / $DATANODES_NUMBER => $(((worker+2)/DATANODES_NUMBER)) containers used" 

# TODO
#echo "Num Compute Threads = $(((core/((worker+2)/DATANODES_NUMBER))))"


echo "\n$core cores used * $worker workers used * 4 multiplication factor => $((core*worker*4)) partitions used"
# NB Il totale delle partizioni sopra calcolato e impostato nel parametro giraph.userPartitionCount viene suddiviso da giraph tra i workers. Difatti nel log di ogni worker compare un messaggio simile al seguente:
# INFO graph.GraphTaskManager: execute: 32 partitions to process with 8 compute thread(s), originally 8 thread(s) on superstep 1
# Nell'esempio di log riportato sopra, moltiplicando le 32 partizioni per 8 threads si ottiene il numero totale di partizioni, impostato tramite il parametro giraph.userPartitionCount, con il quale è stato suddiviso/partizionato il grafo in input.



#echo "\nEnter useOutOfCoreGraph (true/false):"
#read useOutOfCoreGraph

#echo "\nEnter maxPartitionsInMemory number:"
#read maxPartitionsInMemory

#echo "\nEnter useOutOfCoreMessages (true/false):"
#read useOutOfCoreMessages

#echo "\nEnter maxMessagesInMemory number:"
#read maxMessagesInMemory

#echo "\nEnter a multiply factor of netty parameters: (1, 2, 3 ...)"
#read nettyFactor







######################################################################################################################
# INIZIO Parametri specifici per YARN presi da ~/Downloads/GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/conf$
# 
# 1)
# -yh 1024 (1024 is the default value)
# --yarnheap 1024
# -ca giraph.yarn.task.heap.mb=1024
#  user-configurable heap memory per worker, default value 1024 MBs
#
# NOTES: 
# - this value is per single worker as specified here: 
#   http://giraph.apache.org/options.html
#   giraph.yarn.task.heap.mb 	integer 	1024 	Name of Giraph property for user-configurable heap memory per worker
# - From gam.log:
#   INFO yarn.GiraphApplicationMaster: Conatain launch Commands :java -Xmx28416M -Xms28416M -cp .:${CLASSPATH}
#   org.apache.giraph.yarn.GiraphYarnTask 1544393256206 3 6 1 1><LOG_DIR>/task-6-stdout.log 2><LOG_DIR>/task-6-stderr.log

# 2)
# -ca giraph.pure.yarn.job=true
#  Is this a pure YARN job (i.e. no MapReduce layer managing Giraph tasks)"

# 3)
# http://giraph.apache.org/options.html
# -ca giraph.yarn.libjars   conf key for comma-separated list of jars to export to YARN workers (NB da prove effettuate non è comma , ma ;)
# oppure
# -yj 
#
# SEGUE L'ELENCO DELLE PROVE FATTE MA NESSUNA HA FUNZIONATO INOLTRE ANCHE CERCANDO SU GOOGLE ALTRI UTENTI NON SONO RIUSCITI A FAR FUNZIONARE
# QUESTO PARAMETRO. L'ERRORE RISCONTRATO E' SEMPRE IL SEGUENTE IN gam-stderr:
# Error: Could not find or load main class org.apache.giraph.yarn.GiraphApplicationMaster
# 
# PROVA 3.1)
#-ca giraph.yarn.libjars="giraph-1.3.0-SNAPSHOT-for-hadoop-2.8.4-jar-with-dependencies.jar;4Profiles-0.0.1-SNAPSHOT.jar" \
#
# PROVA 3.2)
#-ca giraph.yarn.libjars="/user/ubuntu/giraph-1.3.0-SNAPSHOT-for-hadoop-2.8.4-jar-with-dependencies.jar;/user/ubuntu/4Profiles-0.0.1-SNAPSHOT.jar" \
#
# NOTA: Il jar E' STATO TROVATO GRAZIE A QUESTO COMANDO:
#ubuntu@cri:~/Downloads/GIRAPH_GIT$ find . -name "*with-dependencies*"
#./giraph/giraph-core/target/giraph-1.3.0-SNAPSHOT-for-hadoop-2.8.4-jar-with-dependencies.jar
#
# FINE Parametri specifici per YARN
######################################################################################################################




######################################################################################################################
# INIZIO OTTIMIZZAZIONE DELLE PARTIZIONI:
# TODO 1)
#
# -ca giraph.graphPartitionerFactoryClass=org.apache.giraph.partition.HashRangePartitionerFactory \
# -ca giraph.partitionClass=org.apache.giraph.partition.ByteArrayPartition \
#
# 1.1)
# https://www.mail-archive.com/user@giraph.apache.org/msg02961.html
#
# 1.2)
# Dal paragrafo 5.4 del libro "Large_Scale_Graph_Processing_Using_Apache_Giraph":
#
# Giraph provides three partitioning techniques: HashPartitionerFactory,
# HashRangePartitionerFactory, LongMappingStorePartitioner
# Factory, SimpleIntRangePartitionerFactory and SimpleLong
# RangePartitionerFactory. The default partitioning method is Hash
# PartitionerFactory, where it uses the vertex ID hash to define its partition.
# This partitioning is the fastest and simplest but does not necessarily guarantee distribution
# balance between different partitions. HashRangePartitionerFactory,
# on the other hand, provide distribution balance between workers where it uses
# range partitioning on the hash values of the vertices to distribute them. The other
# three partitioning techniques (LongMappingStorePartitionerFactory,
# SimpleIntRangePartitionerFactory and SimpleLongRange
# PartitionerFactory) are subclasses of the abstract class Simple
# Partition, where they are all based on range partitioning on the actual vertex
# ID instead of its hash value. Note that all partitioning techniques are based
# on mathematical calculation to statically assign the location of the vertices and
# partitions in Giraph.


# 2)
# 
# https://www.mail-archive.com/search?q=giraph.userPartitionCount&l=user%40giraph.apache.org
# 
# -ca giraph.userPartitionCount
#
# 2.1)
# https://www.mail-archive.com/user@giraph.apache.org/msg02841.html
# ... You can also
# play with the number of partitions (using giraph.userPartitionCount) and
# set it to a multiple of the number of physical cores. For instance if your
# processor has 16 cores, try 16*4=128 partitions to take advantage of core
# oversubscription.
# ATTENZIONE POICHE' IN QUESTO LINK TRATTANO UN SOLO WORKER IN PSEUDO DISTRIBUTED MODE
# INVECE NEL LINK SEGUENTE TRATTANO DI UN CLUSTER E INFATTI NEL CALCOLO DI userPartitionCount
# CONSIDERANO ANCHE IL NUMERO DEI WORKERS
# 
# 2.2)
# https://www.mail-archive.com/user@giraph.apache.org/msg02324.html
# N_PARTITIONS=3*N_THREADS*N_WORKERS
#
# 2.3)
# http://mail-archives.apache.org/mod_mbox/giraph-user/201409.mbox/%3CCAPeVUvaphpchKpwWBypku-x07TybxqzY3TK+v=wXnPnh20O9Tg@mail.gmail.com%3E
# giraph.userPartitionCount : Chooses the number of partition. Default is #
# workers squared. In your case if you do 4 workers then that gives you 16
# partitions, 4 per worker, which means that if you do numComputeThreads=4
# you'll have one partition to process per thread. You might want to consider
# using a higher number of partitions (maybe 3 per thread, or higher) since
# partitions are processed in parallel in vertex computation. That way, when
# partitions are finer-grained, if you have one partition eating a bunch of
# time the others can keep getting processed by the remaining threads.
#
######################################################################################################################







# INIZIO MKDIR DIRECTORY DI SESSIONE DI OUTPUT ###################################################

dateDir="$(date +'%Y-%m-%d')"
dateTimeDir="$(date +'%Y-%m-%d_%H-%M-%S')"
dateTimeDirInput="$dateTimeDir--$x--$z"
execDir=""
mkdir -p OUTPUTS/$dateDir

echo "\nls OUTPUTS/$dateDir"
ls OUTPUTS/$dateDir

echo "\nEnter existing session directory name or 'N' to create after a new session directory"
read Session

if [ $Session = "N" ]
then
	echo "\nEnter new session directory:"
	read Session2
	execDir="OUTPUTS/$dateDir/$dateTimeDir--SESSION--$Session2/$dateTimeDirInput"
else
	execDir="OUTPUTS/$dateDir/$Session/$dateTimeDirInput"
fi

mkdir -p $execDir

echo ""

# FINE MKDIR DIRECTORY DI SESSIONE DI OUTPUT ###################################################



# INIZIO SPIEGAZIONI PARAMETRI RELATIVI A zookeeper ############################################
#~/4-profiles-scripts/0_SETUP/CONF/giraph/LEGGIMI_zoo_cfg.txt
# FINE SPIEGAZIONI PARAMETRI RELATIVI A zookeeper ##############################################





# INIZIO SPIEGAZIONI RELATIVE AI PARAMETRI giraph.numInputThreads=1 e giraph.numOutputThreads=1 e giraph.numComputeThread ###################
#https://stackoverflow.com/questions/39155659/giraph-numinputthreads-execution-time-for-input-superstep-its-the-same-using


#NOTA BENE - Inizialmente avevo impostato i tre parametri giraph.numInputThreads=$core e giraph.numOutputThreads=$core e giraph.numComputeThread=$core come indicato nel punto 5 sotto riportato. In seguito facendo un'analisi più attenta e leggendo il link di stackoverflow sopra riportato nel mio caso i parametri giraph.numInputThreads=1 e giraph.numOutputThreads=1 devono essere impostati a 1

#Sempre un'analisi più attenta mi ha portato a impostare il parametro giraph.numComputeThread (definito come il numero di computation thread per ogni worker) in questo modo: giraph.numComputeThreads = (numero di core / numero containers)-1 dove:
# - numero di container è stabilito indirettamente in questo modo:
#   (1 application master + 1 giraph master + numero di Giraph Workers) / numero di data nodes = numero Containers
# - il valore -1 serve per lasciare un thread al lavoro interno di giraph, come indicato da Claudio Martella nel seguente post: http://mail-archives.apache.org/mod_mbox/giraph-user/201409.mbox/%3CCAPeVUvaphpchKpwWBypku-x07TybxqzY3TK+v=wXnPnh20O9Tg@mail.gmail.com%3E

# 5
# https://lists.apache.org/thread.html/3a40548fe1dc6c608828f1280c37189b475becfd35b57e8a148989fe@1430322601@%3Cuser.giraph.apache.org%3E
# Hey Arjun, I am glad someone finally responded to this thread. I am surprised no one else is trying to figure out these configuration settings... Here is my understanding of your questions (though I am not sure they are right): *Is setting both mapreduce.map.cpu.vcores and yarn.nodemanager.resource.cpu-vcores is required?* Yes, I believe you need both of these set or else they will revert to default values. Importantly, I think you should set these to the same value so that you spawn one mapper/giraph-worker per machine (as this was said to be optimal). Since I have 32 cores per machine, I have set both these values to 32 and has worked to only spawn one worker per machine (unless I try to have a worker share a machine with the master). Check this page out: http://blog.cloudera.com/blog/2014/04/apache-hadoop-yarn-avoiding-6-time-consuming-gotchas/ *What happens if they are not set, while giraph.numComputeThreads is set?* The above parameters specify how many nodes per machine you are allowing for workers AND how many cores one worker will use. If you don't set *giraph.numComputeThreads *then the worker will use the default number (I think that is 1) despite possibly being allocated more cores. Hence, I set *giraph.numComputeThreads, **giraph.numInputThreads, *and *giraph.numOutputThreads *to be the same as the above two paramters, the total cores in one machine (for me 32). Giraph is never going to fully utilize the entire machine, so I don't think its really possible to tell if these are correct settings, but all of this seems reasonable based on my experience and how these parameters are defined. *Are there any other parameters that must be set in order to make sure we are *really* using the cores, not just multi-threading on a single core?* No idea, but the above parameters and some memory configurations are all I set. The memory configurations are worse in my opinion, as I was running into memory issues and ended up having to manually set the following parameters: - yarn.nodemanager.resource.memory-mb - yarn.scheduler.minimum-allocation-mb - yarn.scheduler.maximum-allocation-mb - mapreduce.map.memory.mb - -yh (in Giraph arguments) All of these were required to be manually set to get Giraph to run without having memory issues. Best regards, Steve

# FINE SPIEGAZIONI RELATIVE AI PARAMETRI giraph.numInputThreads=1 e giraph.numOutputThreads=1 e giraph.numComputeThread ###################





# INIZIO SPIEGAZIONI RELATIVE AL PARAMETRO giraph.useInputSplitLocality 	
#giraph.useInputSplitLocality 	
#default value 	true 	
#To minimize network usage when reading input splits, each worker can prioritize splits that reside on its host. This, however, comes at the cost of increased load on ZooKeeper. Hence, users with a lot of splits and input threads (or with configurations that can't exploit locality) may want to disable it.
# FINE SPIEGAZIONI RELATIVE AL PARAMETRO giraph.useInputSplitLocality 	




time -v -o $execDir/$x--$z--log giraph $JAR_DIR/$FOUR_PROFILES_JAR it.uniroma1.di.fourprofiles.worker.superstep0.gas1.Worker_Superstep0_GAS1 \
-ca customInfoLogLevel=$cill \
-ca giraph.logThreadLayout=true \
-yh $h \
-ca giraph.pure.yarn.job=true \
-ca giraph.master.observers=it.uniroma1.di.fourprofiles.master.observer.Observer_FourProfiles \
-mc it.uniroma1.di.fourprofiles.master.Master_FourProfiles \
-ca io.edge.reverse.duplicator=true \
-eif it.uniroma1.di.fourprofiles.io.format.IntEdgeData_TextEdgeInputFormat_ReverseEdgeDuplicator \
-eip INPUT_GRAPHS/$x \
-vof org.apache.giraph.io.formats.IdWithValueTextOutputFormat \
-op output \
-w $worker \
-ca isolatedVertexNumber=$ivn \
-ca $z=true \
-ca giraph.SplitMasterWorker=true \
-ca giraph.messageCombinerClass=it.uniroma1.di.fourprofiles.worker.msgcombiner.Worker_MsgCombiner \
-ca giraph.metrics.enable=$metrics \
-ca giraph.numComputeThreads=$core \
-ca giraph.numInputThreads=1 \
-ca giraph.numOutputThreads=1 \
-ca giraph.useInputSplitLocality=true \
-ca giraph.useBigDataIOForMessages=true \
-ca giraph.useMessageSizeEncoding=true \
-ca giraph.oneToAllMsgSending=true \
-ca giraph.isStaticGraph=true \
-ca giraph.jmap.histo.enable=false \
-ca giraph.userPartitionCount=$((core*worker*4)) \
-ca giraph.minPartitionsPerComputeThread=$((worker*4)) \
-ca giraph.zkSessionMsecTimeout=600000 \
-ca giraph.zKMinSessionTimeout=60000000 \
-ca giraph.zkMaxSessionTimeout=90000000 \


#TODO provoca il FAILED del run, verificarne il motivo
#-ca giraph.trackJobProgressOnClient 	boolean 	false 	Whether to track job progress on client or not

#-ca giraph.numOutOfCoreThreads=$core \
#-ca giraph.useOutOfCoreGraph=$useOutOfCoreGraph \
#-ca giraph.maxPartitionsInMemory=$maxPartitionsInMemory \
#-ca giraph.useOutOfCoreMessages=$useOutOfCoreMessages \
#-ca giraph.maxMessagesInMemory=$maxMessagesInMemory \




#TODO I parametri zKMinSessionTimeout e zkMaxSessionTimeout devono essere testati
#-ca giraph.zKMinSessionTimeout=60000000 \
#-ca giraph.zkMaxSessionTimeout=90000000 \

#TODO verificare il motivo per il quale utilizzando questi parametri l'output viene creato solamente dal primo worker, lo si evince dal fatto
#che l'unico file che ha dimensione diversa da zero è /user/ubuntu/output/part-m-00001
#-ca giraph.graphPartitionerFactoryClass=org.apache.giraph.partition.HashRangePartitionerFactory \
#-ca giraph.partitionClass=org.apache.giraph.partition.ByteArrayPartition \

#-ca giraph.nettyAutoRead=true \
#-ca giraph.useNettyDirectMemory \
#-ca giraph.useNettyPooledAllocator \
#-ca giraph.channelsPerServer=$((nettyFactor*1)) \
#-ca giraph.nettyClientThreads=$((nettyFactor*4)) \
#-ca giraph.nettyClientExecutionThreads=$((nettyFactor*8)) \
#-ca giraph.nettyServerThreads=$((nettyFactor*16)) \
#-ca giraph.nettyServerExecutionThreads=$((nettyFactor*8)) \
#-ca giraph.clientSendBufferSize=$((nettyFactor*524288)) \
#-ca giraph.clientReceiveBufferSize=$((nettyFactor*32768)) \
#-ca giraph.serverSendBufferSize=$((nettyFactor*32768)) \
#-ca giraph.serverReceiveBufferSize=$((nettyFactor*524288)) \
#-ca giraph.vertexRequestSize=$((nettyFactor*524288)) \
#-ca giraph.edgeRequestSize=$((nettyFactor*524288)) \
#-ca giraph.msgRequestSize=$((nettyFactor*524288)) \
#-ca giraph.nettyRequestEncoderBufferSize=$((nettyFactor*32768)) \


#APPUNTI:

# 1 
# http://mail-archives.apache.org/mod_mbox/giraph-user/201402.mbox/%3CBAY176-W8734DBDAE7401ED04848FBE960@phx.gbl%3E
# capire come lanciare il programma allOption che stampa tutti i parametri con i rispettivi valori di default

# 2 
# https://svn.apache.org/repos/asf/giraph/trunk/giraph/src/main/java/org/apache/giraph/GiraphConfiguration.java
# Da qui sono stati presi tutti i valori di default dei parametri di NETTY

# 3 
# http://giraph.apache.org/ooc.html
# -ca giraph.isStaticGraph=true \
# For computations where the graph is static, meaning that no edges or vertices are added or removed during the computation (e.g. PageRank, SSSP, etc.), it is a waste of i/o operations to write back to disk the adjacency list of each vertex. With parameter "giraph.isStaticGraph=true" (disabled by default), when offloading a partition to disk, Giraph will write back only the vertices' values, saving the i/o produced by writing back also the the adjacency lists.


# 4 
# Per controllare la correttezza dei valori di default è stato eseguito un grep come mostrato di seguito:
#
# ~/Downloads/GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/conf$ cat * | grep clientSendBufferSize
#      new IntConfOption("giraph.clientSendBufferSize", 512 * ONE_KB,
#
# ~/Downloads/GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/conf$ cat * | grep serverReceiveBufferSize
#      new IntConfOption("giraph.serverReceiveBufferSize", 512 * ONE_KB,
#
# ~/Downloads/GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/conf$ cat * | grep edgeRequestSize
#      new IntConfOption("giraph.edgeRequestSize", 512 * ONE_KB,





echo "\n\n"

hadoop fs -ls /user/ubuntu/output

hdfs dfs -get /user/ubuntu/output $execDir

echo "\n\n"


#https://stackoverflow.com/questions/556405/what-do-real-user-and-sys-mean-in-the-output-of-time1/556411#556411
cat $execDir/$x--$z--log

echo "\n\nRefer to 'Elapsed (wall clock) time'. For an explanation of other times see https://stackoverflow.com/questions/556405/what-do-real-user-and-sys-mean-in-the-output-of-time1/556411#556411"
echo "\nComplete output available on '$execDir'"
