#variabili di ambiente impostate dentro .bashrc
#export FOUR_PROFILES_JAR=4Profiles-0.0.1-SNAPSHOT.jar
#export RANDOMGRAPHGEN_WORKSPACE=/home/user1/git/random-graph-generator/RandomGraphGenerator   #dove c'è il pom.xml
#export FOUR_PROFILES_SCRIPTS=$HOME/4-profiles-scripts





######################################INIZIO ISTRUZIONI MAVEN###########################################################
echo "Digitare (1 oppure 2):"
echo "1, per compilare il jar nella macchina linux con utente principale ubuntu"
echo "2, per compilare il jar con istruzioni specifiche per la mia macchina virtuale dove il sorgente si trova in user1 e gli scripts in ubuntu"
read i



if [ $i = "1" ] 
then
	cd $RANDOMGRAPHGEN_WORKSPACE #bisogna entrare nella directory radice dove c'è il pom.xml prima di lanciare il comando mvn
	mvn clean install
	cp $RANDOMGRAPHGEN_WORKSPACE/target/$RANDOMGRAPHGEN_JAR $JAR_DIR
fi

if [ $i = "2" ] 
then
	#istruzioni specifiche per il mio notebook in cui il workspace si trova nella home dell'utente user1 mentre 
	#la directory 4-profiles-scripts si trova nella home dell'utente ubuntu
	cd /home/user1/git/random-graph-generator/RandomGraphGenerator
	sudo -u user1 bash -c 'mvn clean install'
	sudo cp /home/user1/git/random-graph-generator/RandomGraphGenerator/target/$RANDOMGRAPHGEN_JAR $JAR_DIR

fi

######################################FINE ISTRUZIONI MAVEN##############################################################
echo "\n$RANDOMGRAPHGEN_JAR updated in $JAR_DIR"

sudo chown -R ubuntu:ubuntu $FOUR_PROFILES_SCRIPTS
sudo chmod -R 755 $FOUR_PROFILES_SCRIPTS
