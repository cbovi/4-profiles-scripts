# 1) Installazione SSH
sudo apt-get install ssh


# 2) questo comando crea i files 
#    $HOME/.ssh/id_rsa
#    $HOME/.ssh/id_rsa.pub
ssh-keygen -f ~/.ssh/id_rsa -t rsa -P ""
#Generating public/private rsa key pair.
#Your identification has been saved in /home/ubuntu/.ssh/id_rsa.
#Your public key has been saved in /home/ubuntu/.ssh/id_rsa.pub.
#The key fingerprint is:
#SHA256:yTwT61l1yXFOxdKbWikiL8GJPLucJPLpqu2e4uDoxiw ubuntu@francesco
#The key's randomart image is:
#+---[RSA 2048]----+
#|              .o=|
#|             ..*o|
#|      . + . . +.=|
#|       * O o o = |
#|        S = . +  |
#|   . . + * . .   |
#|+   o = = .      |
#|E=. .o +         |
#|*==*o.           |
#+----[SHA256]-----+


# 3) questo comando crea i files 
#    $HOME/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys


# 4)
# Infine il file $HOME/.ssh/known_hosts viene generato IN AUTOMATICO a seguito dei seguenti messaggi quando si avvia hadoop 
# in modalità pseudo-distributed per la prima volta in assoluto:
#
# The authenticity of host 'localhost (127.0.0.1)' can't be established.
# ECDSA key fingerprint is SHA256:l0tNFKwlDiWadaLjufHQw6RFgal9ibxWdqRrcvZLZ1k.
# Are you sure you want to continue connecting (yes/no)? yes
# localhost: Warning: Permanently added 'localhost' (ECDSA) to the list of known hosts.
#
#
# The authenticity of host '0.0.0.0 (0.0.0.0)' can't be established.
# ECDSA key fingerprint is SHA256:l0tNFKwlDiWadaLjufHQw6RFgal9ibxWdqRrcvZLZ1k.
# Are you sure you want to continue connecting (yes/no)? yes
# 0.0.0.0: Warning: Permanently added '0.0.0.0' (ECDSA) to the list of known hosts.
# 



# 5) 
# Copia del file config necessario per accedere alle macchine EC2 in modalità password less
#TODO CORREGGERE L'ERRORE INVALID OPTION cp: invalid option -- 'O'

cp $FOUR-PROFILES-SCRIPTS/0_SETUP/CONF/ssh/config $HOME/.ssh


# 6)
# Per accedere alle macchine EC2 è necessario copiare il file .pem nella cartella .ssh
echo "NOTA: Per accedere alle macchine EC2 è necessario copiare nella directory $HOME/.ssh il file .pem contenente la chiave"
echo "      privata. Dopo averlo copiato occorre eseguire i seguenti comandi:"
echo ""
echo "      sudo chown ubuntu:ubuntu $HOME/.ssh/nomefile.pem"
echo "      sudo chmod 600 $HOME/.ssh/nomefile.pem"

