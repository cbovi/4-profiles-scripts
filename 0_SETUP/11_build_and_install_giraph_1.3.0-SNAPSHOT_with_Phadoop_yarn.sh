echo $HADOOP_VERSION

cd $GIRAPH_GIT/giraph

mvn clean

#Compilazione di Giraph 1.3.0-SNAPSHOT per Phadoop_yarn e applicazioni di tipo yarn
mvn -Phadoop_yarn -fae -Dhadoop.version=$HADOOP_VERSION -DskipTests clean package -DskipITs -Dcheckstyle.skip

sudo rm -R /usr/local/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-Phadoop_yarn

# copia della directory di giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION in /usr/local e aggiunta del suffisso "-Phadoop_yarn"
sudo cp -R $GIRAPH_GIT/giraph/giraph-dist/target/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-bin/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION /usr/local/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-Phadoop_yarn


sudo chown -R ubuntu:ubuntu /usr/local/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-Phadoop_yarn
sudo chmod -R 755 /usr/local/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-Phadoop_yarn
