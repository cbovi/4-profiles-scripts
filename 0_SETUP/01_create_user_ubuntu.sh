#usare il comando adduser e non il comando useradd poiché quest'ultimo non crea la home, non chiede la pwd ecc.
sudo adduser ubuntu

#questo comando serve per dare la possibilità all'utente ubuntu di usare il comando sudo
#https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-ubuntu-quickstart
usermod -aG sudo ubuntu


#una volta creato l'utente ubuntu per loggarsi in modo da entrare automaticamente nella sua home occorre usare il seguente comando:
#su - ubuntu
