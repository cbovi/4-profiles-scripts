cd $HOME
mkdir Downloads
cd Downloads
mkdir GIRAPH_DOWNLOADS
cd GIRAPH_DOWNLOADS

#check
pwd

#Download della versione 1.2 di Giraph predisposta per Hadoop2 e applicazioni di tipo mapreduce
#(per ottenere una versione di Giraph 1.2 per Hadoop2 e applicazioni di tipo yarn si dovrebbe 
#scaricare il sorgente di Giraph 1.2 e compilarlo con il profilo -Phadoop_yarn) 
wget https://archive.apache.org/dist/giraph/giraph-1.2.0/giraph-dist-1.2.0-hadoop2-bin.tar.gz

tar -xvzf giraph-dist-1.2.0-hadoop2-bin.tar.gz

#notare che dalla decompressione del tar.gz si ottiene una directory che ha un nome diverso dal tar.gz cioè giraph-1.2.0-hadoop2-for-hadoop-2.5.1
sudo cp -R giraph-1.2.0-hadoop2-for-hadoop-2.5.1 /usr/local

sudo chown -R ubuntu:ubuntu /usr/local/giraph-1.2.0-hadoop2-for-hadoop-2.5.1
sudo chmod -R 755 /usr/local/giraph-1.2.0-hadoop2-for-hadoop-2.5.1
