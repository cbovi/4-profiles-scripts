sudo apt-get install maven

#Dopo aver instalato maven è necessario aggiungere nel file .bashrc le seguenti variabili di ambiente tramite l'apposito script 02_set_bashrc_env_vars.sh.

#START MAVEN########################################################################
#Per utilizzare maven installato tramite "sudo apt-get install maven"
#export M2_HOME=/usr/share/maven
#export MAVEN_HOME=/usr/share/maven
#export M2=$M2_HOME/bin
#export PATH=$PATH:$M2
#END MAVEN##########################################################################
