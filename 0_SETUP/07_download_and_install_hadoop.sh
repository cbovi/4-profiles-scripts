cd $HOME
mkdir Downloads
cd Downloads
mkdir HADOOP_DOWNLOADS
cd HADOOP_DOWNLOADS

#check
pwd

wget https://archive.apache.org/dist/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz

tar -xvzf hadoop-$HADOOP_VERSION.tar.gz

sudo cp -R hadoop-$HADOOP_VERSION /usr/local

sudo chown -R ubuntu:ubuntu /usr/local/hadoop-$HADOOP_VERSION
sudo chmod -R 755 /usr/local/hadoop-$HADOOP_VERSION
