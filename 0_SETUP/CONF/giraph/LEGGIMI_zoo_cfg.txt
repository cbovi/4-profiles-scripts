Il file zoo.cfg è stato configurato seguendo le istruzioni contenute nel link https://zookeeper.apache.org/doc/r3.4.13/zookeeperAdmin.html#sc_troubleshooting, nello specifico:


1) 
maxClientCnxns=16384, è stato aumentato notevolmente rispetto al valore di default 60

Segue la documentazione relativa a questo parametro presa da https://zookeeper.apache.org/doc/r3.4.13, notare che questo parametro non è configurabile tramite le opzioni di giraph http://giraph.apache.org/options.html

maxClientCnxns (No Java system property)
Limits the number of concurrent connections (at the socket level) that a single client, identified by IP address, may make to a single member of the ZooKeeper ensemble. This is used to prevent certain classes of DoS attacks, including file descriptor exhaustion. The default is 60. Setting this to 0 entirely removes the limit on concurrent connections.


NOTA BENE:
maxClientCnxns è stato configurato solamente nel file zoo.cfg poiché da http://giraph.apache.org/options.html e ALLOPTIONS non sembra che sia stato previsto un analogo parametro di giraph.

2)
minSessionTimeout=60000000
maxSessionTimeout=90000000

anche questi due parametri sono stati aumentati notevolmente rispetto al valore di default, specificato in questa pagina http://giraph.apache.org/options.html e riportato di seguito:

giraph.zKMinSessionTimeout 	integer 	600000 	ZooKeeper minimum session timeout
giraph.zkMaxSessionTimeout 	integer 	900000 	ZooKeeper maximum session timeout


NOTA BENE:
minSessionTimeout=60000000 e maxSessionTimeout=90000000 sono stati configurati sia in zoo.cfg che nel file ~/4-profiles-scripts/6_DISTRIBUTED/SCRIPTS/6_GIRAPH_RUNS/4run_giraph_distributed.sh


3)
-ca giraph.zkSessionMsecTimeout=600000
 
Inoltre nel file ~/4-profiles-scripts/6_DISTRIBUTED/SCRIPTS/6_GIRAPH_RUNS/4run_giraph_distributed.sh è stato configurato questo ulteriore parametro preso da http://giraph.apache.org/options.html, di cui si riporta di seguito la documentazione: 
giraph.zkSessionMsecTimeout 	integer 	60000 	ZooKeeper session millisecond timeout

NOTA BENE:
Non è stato però trovato in https://zookeeper.apache.org/doc/r3.4.13 un analogo paragrafo da configurare nel file zoo.cfg
