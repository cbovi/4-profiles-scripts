#https://stackoverflow.com/questions/33728752/building-giraph-with-hadoop
cp $FOUR_PROFILES_SCRIPTS/0_SETUP/CONF/giraph/pom.xml $GIRAPH_GIT/giraph

#TODO Risolve il problema dei datanode che hanno memoria zero
cp $FOUR_PROFILES_SCRIPTS/0_SETUP/CONF/giraph/GiraphYarnClient.java $GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/yarn

#TODO Sembrerebbe che giraph si blocca su Allocation Failure del GC Monitor solamente quando "time spent on gc" è maggiore di zero
#graph.ComputeCallable: call: Computation took 67.774315 secs for 1 partitions on superstep 1.  Flushing started (time waiting on partitions was 0.00 s, time processing partitions was 62.47 s, time spent on gc was 5.30 s)
#graph.GraphTaskManager: installGCMonitoring: name = PS Scavenge, action = end of minor GC, cause = Allocation Failure, duration = 15ms
#cp $FOUR_PROFILES_SCRIPTS/0_SETUP/CONF/giraph/GraphTaskManager.java $GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/graph


# https://zookeeper.apache.org/doc/r3.4.13/zookeeperAdmin.html#sc_troubleshooting
# Limits the number of concurrent connections (at the socket level) that a single client, identified by IP address, may make to a single member of the ZooKeeper ensemble. This is used to prevent certain classes of DoS attacks, including file descriptor exhaustion. The default is 60. Setting this to 0 entirely removes the limit on concurrent connections.
cp $FOUR_PROFILES_SCRIPTS/0_SETUP/CONF/giraph/zoo.cfg $GIRAPH_GIT/giraph/giraph-gora/conf/



