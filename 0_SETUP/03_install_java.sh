#https://medium.com/coderscorner/installing-oracle-java-8-in-ubuntu-16-10-845507b13343

#con questo primo commando add-“apt” si specifica il sito “webup8team/java” da dove il successivo comando “apt-get install” potrà scaricare e installare java 1.8 di oracle
sudo add-apt-repository ppa:webupd8team/java 

#update (aggiornamento) del sito sopra indicato necessario per far funzionare il successivo comando “apt-get install”
sudo apt-get update 

#commando “effettivo” di installazione
sudo apt-get install oracle-java8-installer

#prova di esecuzione del java compiler con il parametro –version che restituisce in output la versione 1.8 di java
javac –version 


#Una volta installato il java deve essere aggiunta nel file .bashrc, che si trova nella home dell’utente ubuntu (/home/ubuntu accessibile con i comandi cd $HOME oppure cd TILDE), la variabile di ambiente

#JAVA_HOME=percorso dove è stato installato il java 1.8 dai comandi sopra, percorso che viene mostrato dall’output degli stessi comandi sopra ossia

#	JAVA_HOME=/usr/lib/jvm/java-8-oracle

#E’ stato necessario installare la jdk di java 1.8 poiché solamente con la jdk è possibile compilare correttamente giraph. Invece con la jre 1.8 la compilazione di giraph va in errore.

#NOTA:
#La java 1.8 esiste in due implementazioni: oracle e open. Entrambe forniscono jdk e jre.

