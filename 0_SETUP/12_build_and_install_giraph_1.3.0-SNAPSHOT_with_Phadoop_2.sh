echo $HADOOP_VERSION

cd $GIRAPH_GIT/giraph

mvn clean

#Compilazione di Giraph 1.3.0-SNAPSHOT per Hadoop2 e applicazioni di tipo mapreduce
mvn -Phadoop_2 -fae -Dhadoop.version=$HADOOP_VERSION -DskipTests clean package -DskipITs -Dcheckstyle.skip

sudo rm -R /usr/local/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-Phadoop_2

# copia della directory di giraph-1.3.0-SNAPSHOT-for-$HADOOP_VERSION in /usr/local e aggiunta del suffisso "-Phadoop_2"
sudo cp -R $GIRAPH_GIT/giraph/giraph-dist/target/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-bin/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION /usr/local/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-Phadoop_2

# NOTA la directory di giraph in /usr/local corrisponde alla GIRAPH_HOME
sudo chown -R ubuntu:ubuntu /usr/local/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-Phadoop_2
sudo chmod -R 755 /usr/local/giraph-1.3.0-SNAPSHOT-for-hadoop-$HADOOP_VERSION-Phadoop_2

