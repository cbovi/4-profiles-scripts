#variabili di ambiente impostate dentro .bashrc
#export FOUR_PROFILES_JAR=4Profiles-0.0.1-SNAPSHOT.jar
#export FOUR_PROFILES_WORKSPACE=/home/user1/git/4-profiles/FourProfiles     #dove c'è il pom.xml
#export FOUR_PROFILES_SCRIPTS=$HOME/4-profiles-scripts


######################################INIZIO ISTRUZIONI MAVEN###########################################################
echo "Digitare (1 oppure 2):"
echo "1, per compilare il jar nella macchina linux con utente principale ubuntu"
echo "2, per compilare il jar con istruzioni specifiche per la mia macchina virtuale dove il sorgente si trova in user1 e gli scripts in ubuntu"
read i


if [ $i = "1" ] 
then
	#bisogna entrare nella directory radice dove c'è il pom.xml prima di lanciare il comando mvn
	cd $FOUR_PROFILES_WORKSPACE 
	mvn clean install
	cp $FOUR_PROFILES_WORKSPACE/target/$FOUR_PROFILES_JAR $JAR_DIR
fi

if [ $i = "2" ] 
then
	#istruzioni specifiche per il mio notebook in cui il workspace si trova nella home dell'utente user1 mentre 
	#la directory 4-profiles-scripts si trova nella home dell'utente ubuntu
        cd /home/user1/git/4-profiles/FourProfiles
	sudo -u user1 bash -c 'mvn clean install'
	sudo cp /home/user1/git/4-profiles/FourProfiles/target/$FOUR_PROFILES_JAR $JAR_DIR
fi

######################################FINE ISTRUZIONI MAVEN#############################################################




sudo chown -R ubuntu:ubuntu $FOUR_PROFILES_SCRIPTS
sudo chmod -R 755 $FOUR_PROFILES_SCRIPTS

sudo chown -R ubuntu:ubuntu $GIRAPH_HOME
sudo chmod -R 755 $GIRAPH_HOME

sudo chown -R ubuntu:ubuntu $HADOOP_HOME
sudo chmod -R 755 $HADOOP_HOME

echo "\n\n$FOUR_PROFILES_JAR updated in $JAR_DIR"

echo "\n\nREMEMBER TO LAUNCH ~/4-profiles-scripts/6_DISTRIBUTED/SCRIPTS/4_HADOOP_SETUP_AND_FIRST_START/1cp_yarn_lib.sh IN ORDER TO COPY $FOUR_PROFILES_JAR FROM $JAR_DIR TO $HADOOP_HOME/share/hadoop/yarn/lib\n\n"



