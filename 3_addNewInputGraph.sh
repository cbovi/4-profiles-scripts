echo "Enter:"
echo "1, to download a new input graph from an URL"
echo "2, to copy a new input graph from WINDOWS c:\TEMP"
read x


if [ $x = "1" ]
then

	cd $P
	echo "\nEnter URL:"
	read url

	wget $url
	
	echo "\nFile downloaded from $url to $P:"
	ls $P
	
fi


if [ $x = "2" ]
then
	echo "\nls /media/sf_TEMP"	
	sudo ls /media/sf_TEMP
	echo "\nEnter input file name (wihout path) to be copied in $P"
	read f
	
	sudo cp /media/sf_TEMP/$f $P
	sudo chown ubuntu:ubuntu $P/$f
	sudo chmod 755 $P/$f
	
	echo "\nFile $f copied in $P"
	ls $P
fi


echo "\nNOTES TO UNCOMPRESS ARCHIVES:"
echo "- .tar.gz => tar -xvzf filename.tar.gz"
echo "- .gz => gunzip filename.gz"
echo "- .zip => filename.zip"

