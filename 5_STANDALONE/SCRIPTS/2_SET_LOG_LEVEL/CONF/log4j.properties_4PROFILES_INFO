# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Define some default values that can be overridden by system properties
#,console 
hadoop.root.logger=INFO,RFA,4PROFILES-MASTER,4PROFILES-OBSERVER,4PROFILES-WORKER-SUPERSTEP0-GAS1,4PROFILES-WORKER-SUPERSTEP1,4PROFILES-WORKER-SUPERSTEP1-GAS2,4PROFILES-WORKER-SUPERSTEP1-GAS3,4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1,4PROFILES-WORKER-SUPERSTEP2-GAS4,4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2,4PROFILES-WORKER-MSGCOMBINER




#TODO DA VERIFICARE
#hadoop.log.dir=./log

#TODO DA VERIFICARE (questa proprietà viene sovrascritta dal valore hadoop.log, determinare chi è che sovrascrive questa proprietà)
#hadoop.log.file=hadoop-giraph.log



log4j.additivity.com=false
log4j.additivity.org=false
log4j.additivity.it.uniroma1.di.fourprofiles.master=false
log4j.additivity.it.uniroma1.di.fourprofiles.master.observer=false
log4j.additivity.it.uniroma1.di.fourprofiles.master.superstep2=false
log4j.additivity.it.uniroma1.di.fourprofiles.master.superstep3=false
log4j.additivity.it.uniroma1.di.fourprofiles.worker.superstep0.gas1=false
log4j.additivity.it.uniroma1.di.fourprofiles.worker.superstep1=false
log4j.additivity.it.uniroma1.di.fourprofiles.worker.superstep1.gas2=false
log4j.additivity.it.uniroma1.di.fourprofiles.worker.superstep1.gas3=false
log4j.additivity.it.uniroma1.di.fourprofiles.worker.superstep1.persaggr1=false
log4j.additivity.it.uniroma1.di.fourprofiles.worker.superstep2.gas4=false
log4j.additivity.it.uniroma1.di.fourprofiles.worker.superstep2.persaggr2=false
log4j.additivity.it.uniroma1.di.fourprofiles.worker.msgcombiner=false


# OFF al posto di INFO per evitare che il log da un determinato package venga scritto
log4j.logger.com=INFO, RFA
log4j.logger.org=INFO, RFA
log4j.logger.org.apache.hadoop.util.NativeCodeLoader=INFO, RFA
log4j.logger.it.uniroma1.di.fourprofiles.master=INFO, 4PROFILES-MASTER
log4j.logger.it.uniroma1.di.fourprofiles.master.observer=INFO, 4PROFILES-OBSERVER
log4j.logger.it.uniroma1.di.fourprofiles.master.superstep2=INFO, 4PROFILES-MASTER-SUPERSTEP2
log4j.logger.it.uniroma1.di.fourprofiles.master.superstep3=INFO, 4PROFILES-MASTER-SUPERSTEP3
log4j.logger.it.uniroma1.di.fourprofiles.worker.superstep0.gas1=INFO, 4PROFILES-WORKER-SUPERSTEP0-GAS1
log4j.logger.it.uniroma1.di.fourprofiles.worker.superstep1=INFO, 4PROFILES-WORKER-SUPERSTEP1
log4j.logger.it.uniroma1.di.fourprofiles.worker.superstep1.gas2=INFO, 4PROFILES-WORKER-SUPERSTEP1-GAS2
log4j.logger.it.uniroma1.di.fourprofiles.worker.superstep1.gas3=INFO, 4PROFILES-WORKER-SUPERSTEP1-GAS3
log4j.logger.it.uniroma1.di.fourprofiles.worker.superstep1.persaggr1=INFO, 4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1
log4j.logger.it.uniroma1.di.fourprofiles.worker.superstep2.gas4=INFO, 4PROFILES-WORKER-SUPERSTEP2-GAS4
log4j.logger.it.uniroma1.di.fourprofiles.worker.superstep2.persaggr2=INFO, 4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2
log4j.logger.it.uniroma1.di.fourprofiles.worker.msgcombiner=INFO, 4PROFILES-WORKER-MSGCOMBINER


# Define the root logger to the system property "hadoop.root.logger".
log4j.rootLogger=${hadoop.root.logger}, EventCounter

# Logging Threshold
log4j.threshold=ALL

# Null Appender
log4j.appender.NullAppender=org.apache.log4j.varia.NullAppender



#
# Rolling File Appender - cap space usage at 5gb.
#
hadoop.log.maxfilesize=256MB
hadoop.log.maxbackupindex=20
log4j.appender.RFA=org.apache.log4j.RollingFileAppender
log4j.appender.RFA.File=${hadoop.log.dir}/${hadoop.log.file}

log4j.appender.RFA.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.RFA.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.RFA.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
log4j.appender.RFA.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
#log4j.appender.RFA.layout.ConversionPattern=%d{ISO8601} %-5p %c{2} (%F:%M(%L)) - %m%n



#
# 4PROFILES-MASTER Rolling File Appender
#
log4j.appender.4PROFILES-MASTER=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-MASTER.File=${hadoop.log.dir}/01.4profiles.master.log

log4j.appender.4PROFILES-MASTER.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-MASTER.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-MASTER.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-MASTER.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-MASTER.layout.ConversionPattern=%F:%M(%L) - %m%n



#
# 4PROFILESSUPERSTEP0GAS1 Rolling File Appender
#
log4j.appender.4PROFILES-WORKER-SUPERSTEP0-GAS1=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-WORKER-SUPERSTEP0-GAS1.File=${hadoop.log.dir}/02.4profiles.worker.superstep0.gas1.log

log4j.appender.4PROFILES-WORKER-SUPERSTEP0-GAS1.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-WORKER-SUPERSTEP0-GAS1.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-WORKER-SUPERSTEP0-GAS1.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-WORKER-SUPERSTEP0-GAS1.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-WORKER-SUPERSTEP0-GAS1.layout.ConversionPattern=%F:%M(%L) - %m%n




#
# 4PROFILES-WORKER-SUPERSTEP1 Rolling File Appender
#
log4j.appender.4PROFILES-WORKER-SUPERSTEP1=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-WORKER-SUPERSTEP1.File=${hadoop.log.dir}/03.4profiles.worker.superstep1.log

log4j.appender.4PROFILES-WORKER-SUPERSTEP1.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-WORKER-SUPERSTEP1.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-WORKER-SUPERSTEP1.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-WORKER-SUPERSTEP1.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-WORKER-SUPERSTEP1.layout.ConversionPattern=%F:%M(%L) - %m%n



#
# 4PROFILES-WORKER-SUPERSTEP1-GAS2 Rolling File Appender
#
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS2=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS2.File=${hadoop.log.dir}/04.4profiles.worker.superstep1.gas2.log

log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS2.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS2.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS2.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS2.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS2.layout.ConversionPattern=%F:%M(%L) - %m%n



#
# 4PROFILES-WORKER-SUPERSTEP1-GAS3 Rolling File Appender
#
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS3=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS3.File=${hadoop.log.dir}/05.4profiles.worker.superstep1.gas3.log

log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS3.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS3.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS3.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS3.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-GAS3.layout.ConversionPattern=%F:%M(%L) - %m%n



#
# 4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1 Rolling File Appender
#
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1.File=${hadoop.log.dir}/06.4profiles.worker.superstep1.aggregator1.log

log4j.appender.4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1.layout.ConversionPattern=%F:%M(%L) - %m%n



#
# 4PROFILES-MASTER-SUPERSTEP2 Rolling File Appender
#
log4j.appender.4PROFILES-MASTER-SUPERSTEP2=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-MASTER-SUPERSTEP2.File=${hadoop.log.dir}/07.4profiles.master.superstep2.log

log4j.appender.4PROFILES-MASTER-SUPERSTEP2.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-MASTER-SUPERSTEP2.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-MASTER-SUPERSTEP2.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-MASTER-SUPERSTEP2.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-MASTER-SUPERSTEP2.layout.ConversionPattern=%F:%M(%L) - %m%n



#
# 4PROFILES-WORKER-SUPERSTEP2-GAS4 Rolling File Appender
#
log4j.appender.4PROFILES-WORKER-SUPERSTEP2-GAS4=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-WORKER-SUPERSTEP2-GAS4.File=${hadoop.log.dir}/08.4profiles.worker.superstep2.gas4.log

log4j.appender.4PROFILES-WORKER-SUPERSTEP2-GAS4.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-WORKER-SUPERSTEP2-GAS4.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-WORKER-SUPERSTEP2-GAS4.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-WORKER-SUPERSTEP2-GAS4.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-WORKER-SUPERSTEP2-GAS4.layout.ConversionPattern=%F:%M(%L) - %m%n



#
# 4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2 Rolling File Appender
#
log4j.appender.4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2.File=${hadoop.log.dir}/09.4profiles.worker.superstep2.aggregator2.log

log4j.appender.4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2.layout.ConversionPattern=%F:%M(%L) - %m%n



#
# 4PROFILES-MASTER-SUPERSTEP3 Rolling File Appender
#
log4j.appender.4PROFILES-MASTER-SUPERSTEP3=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-MASTER-SUPERSTEP3.File=${hadoop.log.dir}/10.4profiles.master.superstep3.log

log4j.appender.4PROFILES-MASTER-SUPERSTEP3.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-MASTER-SUPERSTEP3.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-MASTER-SUPERSTEP3.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-MASTER-SUPERSTEP3.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-MASTER-SUPERSTEP3.layout.ConversionPattern=%F:%M(%L) - %m%n




#
# 4PROFILES-OBSERVER Rolling File Appender
#
log4j.appender.4PROFILES-OBSERVER=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-OBSERVER.File=${hadoop.log.dir}/11.4profiles.observer.log

log4j.appender.4PROFILES-OBSERVER.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-OBSERVER.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-OBSERVER.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-OBSERVER.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-OBSERVER.layout.ConversionPattern=%F:%M(%L) - %m%n




#
# 4PROFILES-WORKER-MSGCOMBINER Rolling File Appender
#
log4j.appender.4PROFILES-WORKER-MSGCOMBINER=org.apache.log4j.RollingFileAppender
log4j.appender.4PROFILES-WORKER-MSGCOMBINER.File=${hadoop.log.dir}/12.4profiles.worker.msgcombiner.log

log4j.appender.4PROFILES-WORKER-MSGCOMBINER.MaxFileSize=${hadoop.log.maxfilesize}
log4j.appender.4PROFILES-WORKER-MSGCOMBINER.MaxBackupIndex=${hadoop.log.maxbackupindex}

log4j.appender.4PROFILES-WORKER-MSGCOMBINER.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
#log4j.appender.4PROFILES-WORKER-MSGCOMBINER.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
# %d{ISO8601} %-5p %c{2} 
log4j.appender.4PROFILES-WORKER-MSGCOMBINER.layout.ConversionPattern=%F:%M(%L) - %m%n




#
# Daily Rolling File Appender
#

log4j.appender.DRFA=org.apache.log4j.DailyRollingFileAppender
log4j.appender.DRFA.File=${hadoop.log.dir}/${hadoop.log.file}

# Rollver at midnight
log4j.appender.DRFA.DatePattern=.yyyy-MM-dd

# 30-day backup
#log4j.appender.DRFA.MaxBackupIndex=30
log4j.appender.DRFA.layout=org.apache.log4j.PatternLayout

# Pattern format: Date LogLevel LoggerName LogMessage
log4j.appender.DRFA.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
# Debugging Pattern format
#log4j.appender.DRFA.layout.ConversionPattern=%d{ISO8601} %-5p %c{2} (%F:%M(%L)) - %m%n


#
# console
# Add "console" to rootlogger above if you want to use this 
#

log4j.appender.console=org.apache.log4j.ConsoleAppender
log4j.appender.console.target=System.err
log4j.appender.console.layout=org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern=%d{yy/MM/dd HH:mm:ss} %p %c{2}: %m%n


#
# TaskLog Appender
#

#Default values
hadoop.tasklog.taskid=null
hadoop.tasklog.iscleanup=false
hadoop.tasklog.noKeepSplits=4
hadoop.tasklog.totalLogFileSize=100
hadoop.tasklog.purgeLogSplits=true
hadoop.tasklog.logsRetainHours=12

log4j.appender.TLA=org.apache.hadoop.mapred.TaskLogAppender
log4j.appender.TLA.taskId=${hadoop.tasklog.taskid}
log4j.appender.TLA.isCleanup=${hadoop.tasklog.iscleanup}
log4j.appender.TLA.totalLogFileSize=${hadoop.tasklog.totalLogFileSize}

log4j.appender.TLA.layout=org.apache.log4j.PatternLayout
log4j.appender.TLA.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n

#
# HDFS block state change log from block manager
#
# Uncomment the following to suppress normal block state change
# messages from BlockManager in NameNode.
#log4j.logger.BlockStateChange=WARN

#
#Security appender
#
hadoop.security.logger=INFO,NullAppender
hadoop.security.log.maxfilesize=256MB
hadoop.security.log.maxbackupindex=20
log4j.category.SecurityLogger=${hadoop.security.logger}
hadoop.security.log.file=SecurityAuth-${user.name}.audit
log4j.appender.RFAS=org.apache.log4j.RollingFileAppender 
log4j.appender.RFAS.File=${hadoop.log.dir}/${hadoop.security.log.file}
log4j.appender.RFAS.layout=org.apache.log4j.PatternLayout
log4j.appender.RFAS.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
log4j.appender.RFAS.MaxFileSize=${hadoop.security.log.maxfilesize}
log4j.appender.RFAS.MaxBackupIndex=${hadoop.security.log.maxbackupindex}

#
# Daily Rolling Security appender
#
log4j.appender.DRFAS=org.apache.log4j.DailyRollingFileAppender 
log4j.appender.DRFAS.File=${hadoop.log.dir}/${hadoop.security.log.file}
log4j.appender.DRFAS.layout=org.apache.log4j.PatternLayout
log4j.appender.DRFAS.layout.ConversionPattern=%d{ISO8601} %p %c: %m%n
log4j.appender.DRFAS.DatePattern=.yyyy-MM-dd

#
# hadoop configuration logging
#

# Uncomment the following line to turn off configuration deprecation warnings.
# log4j.logger.org.apache.hadoop.conf.Configuration.deprecation=WARN

#
# hdfs audit logging
#
hdfs.audit.logger=INFO,NullAppender
hdfs.audit.log.maxfilesize=256MB
hdfs.audit.log.maxbackupindex=20
log4j.logger.org.apache.hadoop.hdfs.server.namenode.FSNamesystem.audit=${hdfs.audit.logger}
log4j.additivity.org.apache.hadoop.hdfs.server.namenode.FSNamesystem.audit=false
log4j.appender.RFAAUDIT=org.apache.log4j.RollingFileAppender
log4j.appender.RFAAUDIT.File=${hadoop.log.dir}/hdfs-audit.log
log4j.appender.RFAAUDIT.layout=org.apache.log4j.PatternLayout
log4j.appender.RFAAUDIT.layout.ConversionPattern=%d{ISO8601} %p %c{2}: %m%n
log4j.appender.RFAAUDIT.MaxFileSize=${hdfs.audit.log.maxfilesize}
log4j.appender.RFAAUDIT.MaxBackupIndex=${hdfs.audit.log.maxbackupindex}

#
# mapred audit logging
#
mapred.audit.logger=INFO,NullAppender
mapred.audit.log.maxfilesize=256MB
mapred.audit.log.maxbackupindex=20
log4j.logger.org.apache.hadoop.mapred.AuditLogger=${mapred.audit.logger}
log4j.additivity.org.apache.hadoop.mapred.AuditLogger=false
log4j.appender.MRAUDIT=org.apache.log4j.RollingFileAppender
log4j.appender.MRAUDIT.File=${hadoop.log.dir}/mapred-audit.log
log4j.appender.MRAUDIT.layout=org.apache.log4j.PatternLayout
log4j.appender.MRAUDIT.layout.ConversionPattern=%d{ISO8601} %p %c{2}: %m%n
log4j.appender.MRAUDIT.MaxFileSize=${mapred.audit.log.maxfilesize}
log4j.appender.MRAUDIT.MaxBackupIndex=${mapred.audit.log.maxbackupindex}

# Custom Logging levels

#log4j.logger.org.apache.hadoop.mapred.JobTracker=DEBUG
#log4j.logger.org.apache.hadoop.mapred.TaskTracker=DEBUG
#log4j.logger.org.apache.hadoop.hdfs.server.namenode.FSNamesystem.audit=DEBUG

# Jets3t library
log4j.logger.org.jets3t.service.impl.rest.httpclient.RestS3Service=INFO

#
# Event Counter Appender
# Sends counts of logging messages at different severity levels to Hadoop Metrics.
#
log4j.appender.EventCounter=org.apache.hadoop.log.metrics.EventCounter

#
# Job Summary Appender 
#
# Use following logger to send summary to separate file defined by 
# hadoop.mapreduce.jobsummary.log.file :
# hadoop.mapreduce.jobsummary.logger=INFO,JSA
# 
hadoop.mapreduce.jobsummary.logger=${hadoop.root.logger}
hadoop.mapreduce.jobsummary.log.file=hadoop-mapreduce.jobsummary.log
hadoop.mapreduce.jobsummary.log.maxfilesize=256MB
hadoop.mapreduce.jobsummary.log.maxbackupindex=20
log4j.appender.JSA=org.apache.log4j.RollingFileAppender
log4j.appender.JSA.File=${hadoop.log.dir}/${hadoop.mapreduce.jobsummary.log.file}
log4j.appender.JSA.MaxFileSize=${hadoop.mapreduce.jobsummary.log.maxfilesize}
log4j.appender.JSA.MaxBackupIndex=${hadoop.mapreduce.jobsummary.log.maxbackupindex}
log4j.appender.JSA.layout=org.apache.log4j.PatternLayout
log4j.appender.JSA.layout.ConversionPattern=%d{yy/MM/dd HH:mm:ss} %p %c{2}: %m%n
log4j.logger.org.apache.hadoop.mapred.JobInProgress$JobSummary=${hadoop.mapreduce.jobsummary.logger}
log4j.additivity.org.apache.hadoop.mapred.JobInProgress$JobSummary=false

#
# Yarn ResourceManager Application Summary Log 
#
# Set the ResourceManager summary log filename
yarn.server.resourcemanager.appsummary.log.file=rm-appsummary.log
# Set the ResourceManager summary log level and appender
yarn.server.resourcemanager.appsummary.logger=${hadoop.root.logger}
#yarn.server.resourcemanager.appsummary.logger=INFO,RMSUMMARY

# To enable AppSummaryLogging for the RM, 
# set yarn.server.resourcemanager.appsummary.logger to 
# <LEVEL>,RMSUMMARY in hadoop-env.sh

# Appender for ResourceManager Application Summary Log
# Requires the following properties to be set
#    - hadoop.log.dir (Hadoop Log directory)
#    - yarn.server.resourcemanager.appsummary.log.file (resource manager app summary log filename)
#    - yarn.server.resourcemanager.appsummary.logger (resource manager app summary log level and appender)

log4j.logger.org.apache.hadoop.yarn.server.resourcemanager.RMAppManager$ApplicationSummary=${yarn.server.resourcemanager.appsummary.logger}
log4j.additivity.org.apache.hadoop.yarn.server.resourcemanager.RMAppManager$ApplicationSummary=false
log4j.appender.RMSUMMARY=org.apache.log4j.RollingFileAppender
log4j.appender.RMSUMMARY.File=${hadoop.log.dir}/${yarn.server.resourcemanager.appsummary.log.file}
log4j.appender.RMSUMMARY.MaxFileSize=256MB
log4j.appender.RMSUMMARY.MaxBackupIndex=20
log4j.appender.RMSUMMARY.layout=org.apache.log4j.PatternLayout
log4j.appender.RMSUMMARY.layout.ConversionPattern=%d{ISO8601} %p %c{2}: %m%n

# HS audit log configs
#mapreduce.hs.audit.logger=INFO,HSAUDIT
#log4j.logger.org.apache.hadoop.mapreduce.v2.hs.HSAuditLogger=${mapreduce.hs.audit.logger}
#log4j.additivity.org.apache.hadoop.mapreduce.v2.hs.HSAuditLogger=false
#log4j.appender.HSAUDIT=org.apache.log4j.DailyRollingFileAppender
#log4j.appender.HSAUDIT.File=${hadoop.log.dir}/hs-audit.log
#log4j.appender.HSAUDIT.layout=org.apache.log4j.PatternLayout
#log4j.appender.HSAUDIT.layout.ConversionPattern=%d{ISO8601} %p %c{2}: %m%n
#log4j.appender.HSAUDIT.DatePattern=.yyyy-MM-dd

# Http Server Request Logs
#log4j.logger.http.requests.namenode=INFO,namenoderequestlog
#log4j.appender.namenoderequestlog=org.apache.hadoop.http.HttpRequestLogAppender
#log4j.appender.namenoderequestlog.Filename=${hadoop.log.dir}/jetty-namenode-yyyy_mm_dd.log
#log4j.appender.namenoderequestlog.RetainDays=3

#log4j.logger.http.requests.datanode=INFO,datanoderequestlog
#log4j.appender.datanoderequestlog=org.apache.hadoop.http.HttpRequestLogAppender
#log4j.appender.datanoderequestlog.Filename=${hadoop.log.dir}/jetty-datanode-yyyy_mm_dd.log
#log4j.appender.datanoderequestlog.RetainDays=3

#log4j.logger.http.requests.resourcemanager=INFO,resourcemanagerrequestlog
#log4j.appender.resourcemanagerrequestlog=org.apache.hadoop.http.HttpRequestLogAppender
#log4j.appender.resourcemanagerrequestlog.Filename=${hadoop.log.dir}/jetty-resourcemanager-yyyy_mm_dd.log
#log4j.appender.resourcemanagerrequestlog.RetainDays=3

#log4j.logger.http.requests.jobhistory=INFO,jobhistoryrequestlog
#log4j.appender.jobhistoryrequestlog=org.apache.hadoop.http.HttpRequestLogAppender
#log4j.appender.jobhistoryrequestlog.Filename=${hadoop.log.dir}/jetty-jobhistory-yyyy_mm_dd.log
#log4j.appender.jobhistoryrequestlog.RetainDays=3

#log4j.logger.http.requests.nodemanager=INFO,nodemanagerrequestlog
#log4j.appender.nodemanagerrequestlog=org.apache.hadoop.http.HttpRequestLogAppender
#log4j.appender.nodemanagerrequestlog.Filename=${hadoop.log.dir}/jetty-nodemanager-yyyy_mm_dd.log
#log4j.appender.nodemanagerrequestlog.RetainDays=3
