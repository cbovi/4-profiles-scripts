sh 1rm_output_logs.sh

echo "\nAvailable input files on $I:"
ls $I

echo "\nEnter input file name:"
read x

echo "\nEnter vertex isolated number:"
read ivn


echo "\nEnter mode (3global/4global/4globalFromLocal):"
read z

echo "\nEnter log level: [ 1=DEBUG (only for small input files) | 2=INFO | 3=CONSOLE_ERROR | 4=OFF ]"
read n

echo "\nIncremental custom INFO log levels (INFO log level must be previously set):"
echo "0, no custom INFO log"
echo "1, enable log of vertex id, edges, received messages at supersteps 1 and 2"
echo "2, 1 + enable log of memory usage by every vertex at the end of supersteps 1 and 2"
echo "\nEnter custom INFO log level: 0, 1, 2"
read cill

if [ $n = "1" ]
then
         cp ../2_SET_LOG_LEVEL/CONF/log4j.properties_4PROFILES_DEBUG $C/log4j.properties
fi

if [ $n = "2" ] 
then
         cp ../2_SET_LOG_LEVEL/CONF/log4j.properties_4PROFILES_INFO $C/log4j.properties      
fi

if [ $n = "3" ]
then 
         cp ../2_SET_LOG_LEVEL/CONF/log4j.properties_CONSOLE_ERROR $C/log4j.properties        
fi

if [ $n = "4" ]
then 
         cp ../2_SET_LOG_LEVEL/CONF/log4j.properties_OFF $C/log4j.properties       
fi
  

echo "\nEnter giraph.metrics.enable (true/false):"
read y

# https://stackoverflow.com/questions/6481005/how-to-obtain-the-number-of-cpus-cores-in-linux-from-the-command-line
echo "\nMax number of availables CORES: $(grep -c ^processor /proc/cpuinfo)"

# http://mail-archives.apache.org/mod_mbox/giraph-user/201409.mbox/%3CCAPeVUvaphpchKpwWBypku-x07TybxqzY3TK+v=wXnPnh20O9Tg@mail.gmail.com%3E
# *giraph.numComputeThreads, giraph.numInputThreads, and
#> giraph.numOutputThreads should be set to the number of threads you have
#> available, or potentially n - 1 or something since as Claudio mentions in
#> that email chain I linked to, Giraph is also doing some additional work in
#> the background. By default these are set to 1, so you should change them to
#> increase the use of parallelism.
echo "Enter number of CORES to be used (enter 1 to produce sequential logs in DEBUG mode):"
read c

echo "\n$c cores used * 1 workers used * 4 multiplication factor => $((c*1*4)) partitions used"
# TODO il numero delle partizioni dovrebbe comunque essere automaticamente ridimensionato da giraph poiché nel log compare questo msg da cui si evince che il numero effettivo di partizioni è minore rispetto al numero richiesto di partizioni con il parametro giraph.userPartitionCount
# INFO graph.GraphTaskManager: execute: 32 partitions to process with 8 compute thread(s), originally 8 thread(s) on superstep 1


#echo "\nEnter useOutOfCoreGraph (true/false):"
#read useOutOfCoreGraph

#echo "\nEnter maxPartitionsInMemory number:"
#read maxPartitionsInMemory

#echo "\nEnter useOutOfCoreMessages (true/false):"
#read useOutOfCoreMessages

#echo "\nEnter maxMessagesInMemory number:"
#read maxMessagesInMemory

#echo "\nEnter a multiply factor of netty parameters: (1, 2, 3 ...)"
#read nettyFactor

echo ""

datedir="$(date +'%Y-%m-%d')"
execdate="$(date +'%Y-%m-%d_%H-%M-%S')"
execdir="$execdate--$x--$z"
mkdir -p OUTPUTS/$datedir/$execdir


# OTTIMIZZAZIONE DELLE PARTIZIONI:
# TODO 1)
#
# -ca giraph.graphPartitionerFactoryClass=org.apache.giraph.partition.HashRangePartitionerFactory \
# -ca giraph.partitionClass=org.apache.giraph.partition.ByteArrayPartition \
#
# 1.1)
# https://www.mail-archive.com/user@giraph.apache.org/msg02961.html
#
# 1.2)
# Dal paragrafo 5.4 del libro "Large_Scale_Graph_Processing_Using_Apache_Giraph":
#
# Giraph provides three partitioning techniques: HashPartitionerFactory,
# HashRangePartitionerFactory, LongMappingStorePartitioner
# Factory, SimpleIntRangePartitionerFactory and SimpleLong
# RangePartitionerFactory. The default partitioning method is Hash
# PartitionerFactory, where it uses the vertex ID hash to define its partition.
# This partitioning is the fastest and simplest but does not necessarily guarantee distribution
# balance between different partitions. HashRangePartitionerFactory,
# on the other hand, provide distribution balance between workers where it uses
# range partitioning on the hash values of the vertices to distribute them. The other
# three partitioning techniques (LongMappingStorePartitionerFactory,
# SimpleIntRangePartitionerFactory and SimpleLongRange
# PartitionerFactory) are subclasses of the abstract class Simple
# Partition, where they are all based on range partitioning on the actual vertex
# ID instead of its hash value. Note that all partitioning techniques are based
# on mathematical calculation to statically assign the location of the vertices and
# partitions in Giraph.

# TODO 2)
# 
# https://www.mail-archive.com/search?q=giraph.userPartitionCount&l=user%40giraph.apache.org
# 
# -ca giraph.userPartitionCount
#
# 2.1)
# https://www.mail-archive.com/user@giraph.apache.org/msg02841.html
# ... You can also
# play with the number of partitions (using giraph.userPartitionCount) and
# set it to a multiple of the number of physical cores. For instance if your
# processor has 16 cores, try 16*4=128 partitions to take advantage of core
# oversubscription.
# ATTENZIONE POICHE' IN QUESTO LINK TRATTANO UN SOLO WORKER IN PSEUDO DISTRIBUTED MODE
# INVECE NEL LINK SEGUENTE TRATTANO DI UN CLUSTER E INFATTI NEL CALCOLO DI userPartitionCount
# CONSIDERANO ANCHE IL NUMERO DEI WORKERS
# 
# 2.2)
# https://www.mail-archive.com/user@giraph.apache.org/msg02324.html
# N_PARTITIONS=3*N_THREADS*N_WORKERS
#
# 2.3
# http://mail-archives.apache.org/mod_mbox/giraph-user/201409.mbox/%3CCAPeVUvaphpchKpwWBypku-x07TybxqzY3TK+v=wXnPnh20O9Tg@mail.gmail.com%3E




time -v -o OUTPUTS/$datedir/$execdir/$x--$z--log giraph $JAR_DIR/$FOUR_PROFILES_JAR it.uniroma1.di.fourprofiles.worker.superstep0.gas1.Worker_Superstep0_GAS1 \
-ca customInfoLogLevel=$cill \
-ca giraph.master.observers=it.uniroma1.di.fourprofiles.master.observer.Observer_FourProfiles \
-mc it.uniroma1.di.fourprofiles.master.Master_FourProfiles \
-ca io.edge.reverse.duplicator=true \
-eif it.uniroma1.di.fourprofiles.io.format.IntEdgeData_TextEdgeInputFormat_ReverseEdgeDuplicator \
-eip $FOUR_PROFILES_SCRIPTS/INPUT_GRAPHS/$x \
-vof org.apache.giraph.io.formats.IdWithValueTextOutputFormat \
-op output \
-w 1 \
-ca $z=true \
-ca isolatedVertexNumber=$ivn \
-ca giraph.SplitMasterWorker=false \
-ca giraph.messageCombinerClass=it.uniroma1.di.fourprofiles.worker.msgcombiner.Worker_MsgCombiner \
-ca giraph.metrics.enable=$y \
-ca giraph.numComputeThreads=$c \
-ca giraph.numInputThreads=1 \
-ca giraph.numOutputThreads=1 \
-ca giraph.useInputSplitLocality=true \
-ca giraph.useBigDataIOForMessages=true \
-ca giraph.useMessageSizeEncoding=true \
-ca giraph.oneToAllMsgSending=true \
-ca giraph.isStaticGraph=true \
-ca giraph.jmap.histo.enable=false \
-ca giraph.userPartitionCount=$((c*1*4)) \
-ca giraph.minPartitionsPerComputeThread=$((1*4)) \

#TODO verificare il motivo per il quale utilizzando questi parametri l'output viene creato solamente dal primo worker, lo si evince dal fatto
#che l'unico file che ha dimensione diversa da zero è /user/ubuntu/output/part-m-00001
#-ca giraph.graphPartitionerFactoryClass=org.apache.giraph.partition.HashRangePartitionerFactory \
#-ca giraph.partitionClass=org.apache.giraph.partition.ByteArrayPartition \

#-ca giraph.useOutOfCoreGraph=$useOutOfCoreGraph \
#-ca giraph.maxPartitionsInMemory=$maxPartitionsInMemory \
#-ca giraph.useOutOfCoreMessages=$useOutOfCoreMessages \
#-ca giraph.maxMessagesInMemory=$maxMessagesInMemory \
#-ca giraph.nettyAutoRead=true \
#-ca giraph.channelsPerServer=$((nettyFactor*1)) \
#-ca giraph.nettyClientThreads=$((nettyFactor*4)) \
#-ca giraph.nettyClientExecutionThreads=$((nettyFactor*8)) \
#-ca giraph.nettyServerThreads=$((nettyFactor*16)) \
#-ca giraph.nettyServerExecutionThreads=$((nettyFactor*8)) \
#-ca giraph.clientSendBufferSize=$((nettyFactor*524288)) \
#-ca giraph.clientReceiveBufferSize=$((nettyFactor*32768)) \
#-ca giraph.serverSendBufferSize=$((nettyFactor*32768)) \
#-ca giraph.serverReceiveBufferSize=$((nettyFactor*524288)) \
#-ca giraph.vertexRequestSize=$((nettyFactor*524288)) \
#-ca giraph.edgeRequestSize=$((nettyFactor*524288)) \
#-ca giraph.msgRequestSize=$((nettyFactor*524288)) \
#-ca giraph.nettyRequestEncoderBufferSize=$((nettyFactor*32768)) \


#APPUNTI:

# 1 
# http://mail-archives.apache.org/mod_mbox/giraph-user/201402.mbox/%3CBAY176-W8734DBDAE7401ED04848FBE960@phx.gbl%3E
# capire come lanciare il programma allOption che stampa tutti i parametri con i rispettivi valori di default

# 2 
# https://svn.apache.org/repos/asf/giraph/trunk/giraph/src/main/java/org/apache/giraph/GiraphConfiguration.java
# Da qui sono stati presi tutti i valori di default dei parametri di NETTY

# 3 
# http://giraph.apache.org/ooc.html
# -ca giraph.isStaticGraph=true \
# For computations where the graph is static, meaning that no edges or vertices are added or removed during the computation (e.g. PageRank, SSSP, etc.), it is a waste of i/o operations to write back to disk the adjacency list of each vertex. With parameter "giraph.isStaticGraph=true" (disabled by default), when offloading a partition to disk, Giraph will write back only the vertices' values, saving the i/o produced by writing back also the the adjacency lists.


# 4 
# Per controllare la correttezza dei valori di default è stato eseguito un grep come mostrato di seguito:
#
# ~/Downloads/GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/conf$ cat * | grep clientSendBufferSize
#      new IntConfOption("giraph.clientSendBufferSize", 512 * ONE_KB,
#
# ~/Downloads/GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/conf$ cat * | grep serverReceiveBufferSize
#      new IntConfOption("giraph.serverReceiveBufferSize", 512 * ONE_KB,
#
# ~/Downloads/GIRAPH_GIT/giraph/giraph-core/src/main/java/org/apache/giraph/conf$ cat * | grep edgeRequestSize
#      new IntConfOption("giraph.edgeRequestSize", 512 * ONE_KB,

# 5
# https://lists.apache.org/thread.html/3a40548fe1dc6c608828f1280c37189b475becfd35b57e8a148989fe@1430322601@%3Cuser.giraph.apache.org%3E
# Hey Arjun, I am glad someone finally responded to this thread. I am surprised no one else is trying to figure out these configuration settings... Here is my understanding of your questions (though I am not sure they are right): *Is setting both mapreduce.map.cpu.vcores and yarn.nodemanager.resource.cpu-vcores is required?* Yes, I believe you need both of these set or else they will revert to default values. Importantly, I think you should set these to the same value so that you spawn one mapper/giraph-worker per machine (as this was said to be optimal). Since I have 32 cores per machine, I have set both these values to 32 and has worked to only spawn one worker per machine (unless I try to have a worker share a machine with the master). Check this page out: http://blog.cloudera.com/blog/2014/04/apache-hadoop-yarn-avoiding-6-time-consuming-gotchas/ *What happens if they are not set, while giraph.numComputeThreads is set?* The above parameters specify how many nodes per machine you are allowing for workers AND how many cores one worker will use. If you don't set *giraph.numComputeThreads *then the worker will use the default number (I think that is 1) despite possibly being allocated more cores. Hence, I set *giraph.numComputeThreads, **giraph.numInputThreads, *and *giraph.numOutputThreads *to be the same as the above two paramters, the total cores in one machine (for me 32). Giraph is never going to fully utilize the entire machine, so I don't think its really possible to tell if these are correct settings, but all of this seems reasonable based on my experience and how these parameters are defined. *Are there any other parameters that must be set in order to make sure we are *really* using the cores, not just multi-threading on a single core?* No idea, but the above parameters and some memory configurations are all I set. The memory configurations are worse in my opinion, as I was running into memory issues and ended up having to manually set the following parameters: - yarn.nodemanager.resource.memory-mb - yarn.scheduler.minimum-allocation-mb - yarn.scheduler.maximum-allocation-mb - mapreduce.map.memory.mb - -yh (in Giraph arguments) All of these were required to be manually set to get Giraph to run without having memory issues. Best regards, Steve



echo "\n\n"

mv output OUTPUTS/$datedir/$execdir
echo "\nls OUTPUTS/$datedir/$execdir/output"
ls OUTPUTS/$datedir/$execdir/output

echo "\n\n"


#https://stackoverflow.com/questions/556405/what-do-real-user-and-sys-mean-in-the-output-of-time1/556411#556411
cat OUTPUTS/$datedir/$execdir/$x--$z--log

echo "\n\nRefer to 'Elapsed (wall clock) time'. For an explanation of other times see https://stackoverflow.com/questions/556405/what-do-real-user-and-sys-mean-in-the-output-of-time1/556411#556411"
echo "\nComplete output available on 'OUTPUTS/$datedir/$execdir'"
