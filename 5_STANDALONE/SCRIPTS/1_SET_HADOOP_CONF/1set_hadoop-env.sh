echo "Attenzione, prima di lanciare lo script assicurarsi di aver impostato il numero di RAM GBs da usare nella variabile HADOOP_OPTS in CONF/hadoop-env.sh"
echo "\nProseguire con l'esecuzione dello script? (S/N)"
read n

if [ $n = "S" ]
then
	cp CONF/hadoop-env.sh $HADOOP_HOME/etc/hadoop
	echo "\nIl file CONF/hadoop-env.sh è stato copiato nella directory $HADOOP_HOME/etc/hadoop"

else
        echo "\nLo script non è stato eseguito"
fi



