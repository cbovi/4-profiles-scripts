# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Set Hadoop-specific environment variables here.

# The only required environment variable is JAVA_HOME.  All others are
# optional.  When running a distributed configuration it is best to
# set JAVA_HOME in this file, so that it is correctly defined on
# remote nodes.





#TESI ##########################################################################################################################
# The java implementation to use.
#1° VARIABILE
export JAVA_HOME=/usr/lib/jvm/java-8-oracle
#export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64





# The jsvc implementation to use. Jsvc is required to run secure datanodes.
#export JSVC_HOME=${JSVC_HOME}

export HADOOP_CONF_DIR=${HADOOP_CONF_DIR:-"/etc/hadoop"}

# Extra Java CLASSPATH elements.  Automatically insert capacity-scheduler.
for f in $HADOOP_HOME/contrib/capacity-scheduler/*.jar; do
  if [ "$HADOOP_CLASSPATH" ]; then
    export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:$f
  else
    export HADOOP_CLASSPATH=$f
  fi
done

# The maximum amount of heap to use, in MB. Default is 1000.
#export HADOOP_HEAPSIZE=
#export HADOOP_NAMENODE_INIT_HEAPSIZE=""





## TESI ##########################################################################################################################
# 
# HADOOP DI DEFAULT NELLA MODALITA' STANDALONE UTILIZZA 1g DI RAM CHE PERO' POTREBBE NON ESSERE SUFFICENTE PER ALCUNI INPUT COME
# Gnutella E PROVOCARE QUINDI UNA OutOfMemory Exception, PERTANTO BISOGNA IMPOSTARE HADOOP IN MODO CHE UTILIZZI PIU' DI 1gb DI RAM.
# 
# A TALE SCOPO BISOGNA IMPOSTARE I PARAMETRI -XmxNg e -XmsNg DENTRO LA VARIABILE HADOOP_OPTS NEL FILE $HADOOP_HOME/etc/hadoop/hadoop-env.sh 
#  
# -Xmx3g   memoria massima allocata a 3Giga (3g)
# -Xms3g   memoria iniziale allocata a 3 Giga (3g)
#
# NB: NELLE MODALITA' PSEUDO-DISTRIBUTED E DISTRIBUTED LA QUANTITA' DI MEMORIA DA UTILIZZARE DEVE ESSERE SPECIFICATE
#     NEI FILES mapred-site.xml e yarn-site.xml
#
#
#export HADOOP_OPTS="$HADOOP_OPTS -Djava.net.preferIPv4Stack=true"
export HADOOP_OPTS="$HADOOP_OPTS -Xmx3g -Xms3g -Djava.net.preferIPv4Stack=true"
## TESI ##########################################################################################################################





# Command specific options appended to HADOOP_OPTS when specified
export HADOOP_NAMENODE_OPTS="-Dhadoop.security.logger=${HADOOP_SECURITY_LOGGER:-INFO,RFAS} -Dhdfs.audit.logger=${HDFS_AUDIT_LOGGER:-INFO,NullAppender} $HADOOP_NAMENODE_OPTS"
export HADOOP_DATANODE_OPTS="-Dhadoop.security.logger=ERROR,RFAS $HADOOP_DATANODE_OPTS"

export HADOOP_SECONDARYNAMENODE_OPTS="-Dhadoop.security.logger=${HADOOP_SECURITY_LOGGER:-INFO,RFAS} -Dhdfs.audit.logger=${HDFS_AUDIT_LOGGER:-INFO,NullAppender} $HADOOP_SECONDARYNAMENODE_OPTS"

export HADOOP_NFS3_OPTS="$HADOOP_NFS3_OPTS"
export HADOOP_PORTMAP_OPTS="$HADOOP_PORTMAP_OPTS"

# The following applies to multiple commands (fs, dfs, fsck, distcp etc)
export HADOOP_CLIENT_OPTS="$HADOOP_CLIENT_OPTS"
#HADOOP_JAVA_PLATFORM_OPTS="-XX:-UsePerfData $HADOOP_JAVA_PLATFORM_OPTS"

# On secure datanodes, user to run the datanode as after dropping privileges
export HADOOP_SECURE_DN_USER=${HADOOP_SECURE_DN_USER}












# TESI CRISTINA ################################################################################################
#2° VARIABILE
#2.1° VERSIONE PER PSEUDO-DISTRIBUTED E PER IL CLUSTER
#export HADOOP_ROOT_LOGGER=DEBUG,RFA

#2.2° VERSIONE PER STANDALONE
#Sono degli identificativi dei file di log (configurati in log4j.properties)
export HADOOP_ROOT_LOGGER=INFO,RFA,4PROFILES-MASTER,4PROFILES-WORKER-SUPERSTEP0-GAS1,4PROFILES-WORKER-SUPERSTEP1,4PROFILES-WORKER-SUPERSTEP1-GAS2,4PROFILES-WORKER-SUPERSTEP1-GAS3,4PROFILES-WORKER-SUPERSTEP1-AGGREGATOR1,4PROFILES-WORKER-SUPERSTEP2-GAS4,4PROFILES-WORKER-SUPERSTEP2-AGGREGATOR2,4PROFILES-WORKER-MSGCOMBINER


#3° VARIABILE <= PROBABILMENTE NON SERVE, E' SBAGLIATA E QUINDI ANDRA' RIMOSSA COME IMPOSTAZIONE
# "./output" non funziona poiché giraph richiede che non esista questa directory, invece impostando "./output" viene la directory "output" viene creato da hadoop proprio prima dell'esecuzione di giraph
#export HADOOP_LOG_DIR="./logs"#nemmeno questo va bene perché crea una cartella log in qualsiasi cartella dove si lancia un qualsiasi comando di hadoop

# Where log files are stored.  $HADOOP_HOME/logs by default.
#export HADOOP_LOG_DIR=${HADOOP_LOG_DIR}/$USER

















# Where log files are stored in the secure data environment.
export HADOOP_SECURE_DN_LOG_DIR=${HADOOP_LOG_DIR}/${HADOOP_HDFS_USER}

# The directory where pid files are stored. /tmp by default.
# NOTE: this should be set to a directory that can only be written to by 
#       the user that will run the hadoop daemons.  Otherwise there is the
#       potential for a symlink attack.
export HADOOP_PID_DIR=${HADOOP_PID_DIR}
export HADOOP_SECURE_DN_PID_DIR=${HADOOP_PID_DIR}

# A string representing this instance of hadoop. $USER by default.
export HADOOP_IDENT_STRING=$USER
