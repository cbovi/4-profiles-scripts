#Il punto 1) non è più necessario dopo aver impostato yarn.application.classpath e mapred.application.classpath nei rispettivi files yarn-site.xml e mapred-site.xml

##### 1) ripristino della cartella $HADOOP_HOME/share/hadoop/yarn/lib/ originale

	#https://stackoverflow.com/questions/29001491/could-not-find-or-load-main-class-org-apache-giraph-yarn-giraphapplicationmaster	
	sudo rm -R $HADOOP_HOME/share/hadoop/yarn/lib/
	sudo cp -R ~/Downloads/HADOOP_DOWNLOADS/hadoop-2.8.4/share/hadoop/yarn/lib $HADOOP_HOME/share/hadoop/yarn/


##### 2) soluzione per far funzionare outOfCore
	sudo cp $GIRAPH_HOME/lib/guava-18.0.jar $HADOOP_HOME/share/hadoop/common/lib/
	sudo cp $GIRAPH_HOME/lib/guava-18.0.jar $HADOOP_HOME/share/hadoop/yarn/lib/
	sudo rm $HADOOP_HOME/share/hadoop/common/lib/guava-11.0.2.jar
	sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/guava-11.0.2.jar


##### 3) soluzione per evitare che in continuazione venga stampato a video il messaggio: Class path contains multiple SLF4J bindings
	sudo rm $HADOOP_HOME/share/hadoop/yarn/lib/slf4j-log4j12-1.7.6.jar
	sudo rm $GIRAPH_HOME/lib/slf4j-log4j12-1.7.6.jar


##### 4) cancellazione dell'eventuale hadoop file system (se precedentemente hadoop è stato usato in pseudo-distributed o distributed mode)

	sudo rm -R $HADOOP_HOME/logs
	sudo rm -R $HADOOP_HOME/hdfs
	sudo rm -R $HADOOP_HOME/tmp


##### 5) ripristino dei files di configurazione di hadoop in modalità standalone (ossia dalla directory /etc/hadoop originale)

	# E' necessario cancellare il master poiché non viene usato nella modalità standalone di hadoop
	sudo rm /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop/masters

	sudo cp CONF/hadoop/hadoop-env.sh /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	sudo cp CONF/hadoop/yarn-env.sh /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	sudo cp CONF/hadoop/hdfs-site.xml /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	sudo cp CONF/hadoop/core-site.xml /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop

	#mapred-site.xml.template viene copiato in mapred-site.xml
	sudo cp CONF/hadoop/mapred-site.xml.template /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop/mapred-site.xml

	sudo cp CONF/hadoop/yarn-site.xml /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	sudo cp CONF/hadoop/slaves /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop
	sudo cp CONF/hadoop/log4j.properties /usr/local/hadoop-$HADOOP_VERSION/etc/hadoop





sudo chown -R ubuntu:ubuntu /usr/local/hadoop-$HADOOP_VERSION
sudo chmod -R 755 /usr/local/hadoop-$HADOOP_VERSION



echo ""
echo "NB: Dopo aver eseguito lo script set_STANDALONE occorre:"
echo "1- impostare manualmente la variabile GIRAPH_HOME nel file $HOME/.bashrc"
echo "2- chiudere e riaprire tutte le shell (TODO poiché l'esecuzione di source .bashrc non resetta la variabile PATH e forse anche altre variabili)"
